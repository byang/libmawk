#!/bin/sh

# symbol validation; this script lists globally visible symbols with missing
# prefix and persistent states (global vars)

# ./gloals.sh is in libporty work/c99scripts/globals.sh

list_macros()
{
	awk -v "fn=$1" '
		/^[ \t]*#define[ \t]*/ {
			name=$0
			sub("^[ \t]*#define[ \t]*", "", name)
			sub("[ \t(].*", "", name)
			print "macro", name, "(" fn ":" NR ")"
		}
		
	' < $1
}

(
echo ""
echo "### missing prefix ###"

(for n in *.c
do
	echo $n >&2
	./globals.sh -g -I../.. -I.. -DLMAWK_VER=\"1\" $n
done

# list macros in the headers
for n in *.h
do
	list_macros $n
done
) | awk '($2 ~ "^mawk_") || ($2 ~ "^Mawk_") || ($2 ~ "^libmawk_") || /CLASS extern/ || ($2 == "main") { next } { print $0 }'

echo ""
echo "### persistent state ###"
for n in *.c
do
	./globals.sh -s -I../.. -I.. -DLMAWK_VER=\"1\" $n
done
)
