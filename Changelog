Changelog for libmawk
~~~~~~~~~~~~~~~~~~~~~

libmawk 1.0.2 (released: 2019-06-22, r1318)
	[core] -Fix: C function calls with uninitialized variables: not sure if array or not, delay resolving it until all the script is parsed so we see if the symbol is an array
	[build] -Fix: missing make uninstall
	[doc] -Add: example (7libmawk)
	[doc] -Fix: manual header sections match manual file name sections (submitted by Bdale)

libmawk 1.0.1 (released: 2019-02-26, r1307)
	[build] -Fix: don't daisy-chain so symlinks (debian patch applied)
	[build] -Add: install (3) and (7) manual pages
	[build] -Add: scconfig --libarchdir, for /usr/lib64
	[build] -Add: install awklib in non-arch-dependent lib dir
	[doc] -Add: AUTHORS file
	[doc] -Update: COPYING: fsf address and formatting
	[libmawk] -Add: handle --help and --version
	[da_bin] -Fix: don't depend on sizeof(long) == 4
	[math] -Fix: if the result of non-NaN arithmetics is nan in emulated nan, rather make it DBL_MAX so it doesn't look like NaN
	[math] -Fix: protect all basic math functions from nan input in emulated nan setup
	[math] -Fix: for systems with unsafe NaN: manual atan2 arg validation
	[math] -Fix: print and printf treats nan specially to make sure "nan" is printed on all platforms (including emulated NaN)
	[math] -Fix: emulated nan strtonum: set endptr propery on nan
	[math] -Fix: when there's no safe nan support, manual convert "nan" to nan on str->num conversion
	-Cleanup: replace #warning with portable macro

libmawk 1.0.0 (released: 2018-11-11, r1276)
	[API] -Change: resume capable function call entries
	[build] -Fix: make clean should remove custom C test objects and executables
	[build] -Fix: incomplete install headers list
	[build] -Add: Makefile variable for selecting zmalloc alternatives
	[build] -Add: optional DESTDIR for debian
	[build] -Add: --prefix, --debug, --symbols
	[build] -Add: help
	[build] -Chnage: upgrade to tmpasm and latest version of scconfig
	[build] -Add: optional support for soname
	[build] -Change: use $(MAKE) instead of make in central Makefiles for parallel build
	[build] -Add: detect -rdynamic
	[core] -Change: extend mpow2 for covering new celltypes
	[core] -Cleanup: rename _STOP and _RANGE to _RANGE_STOP and _RANGE_CHK to make their purpose more clear
	[core] -Fix: return read error in getline doesn't fall in infinite loop in fill buff anymore
	[core] -Add: MAWK_MEM_PEDANTIC compile time option; when set, try to free all allocation instead of letting zmalloc catch them all
	[core] -Fix: don't free the hash before freeing argv and others
	[core] -Fix: make sure code blocks are free'd only once, but are really free'd
	[core] -Fix: memory leak when initializing argv
	[core] -Fix: let mawk_delete() free arrays, don't manually do that on argv
	[core] -Change: let mawk_delete() free user function
	[core] -Change: fin is under files - higher level code should do all access through the file infra
	[core] -Fix: FIN is only about buffering, no open/close should go through that API
	[core] -Fix: reference count vf structs since one vf might be open multiple times from FILE_NODEs (for input and output) and should be destroyed only once
	[core] -Fix: closing all files, not only output files, is not a pedantic-mem feature - not doig it could lead to leaking fds and other resources even if zmalloc pools are freed centrally
	[core] -Fix: there is only one way files are closed, only one code is needed
	[core] -Fix: mawk_fill_buff returns a signed long for indicating temporary error (fixed for 64 bit)
	[core] -Fix: rename state field userdata to func_userdata to make the purpose more clear
	[core] -Add: ctx_userdata intended for the host app for storing whatever data, per context
	[core] -Add: have a field in the state struct for the C function being called back so that subsequent execute()s may know about that
	[core] -Fix: memory leak: free data of c function calls compiled into the code at uninit
	[core] -Fix: C89 fixes (func-data ptr conv through union, don't use strcasemp(), strdup, snprintf, //, vararg macros)
	[core] -Fix: wrong arg passed to isnan - cell instead fo double value
	[core] -Fix: vio fclose survives if there's no close function available (double close?)
	[doc] -Add: short desc about the vm
	[doc] -Add: explain how execution/resume works
	[doc] -Add: describe deep recursion
	[doc] -Add: minimalistic install
	[example_apps] -Add: blocking fifo test also tests bifunct getline in an user function
	[example_apps] -Add: make sure to demonstrate how the code is suspended even in user functions
	[example_apps] -Update: 15_call for the new function call API
	[example_apps] -Add: example on run limit resume
	[example_apps] -Add: central Makefile that runs all examples
	[example_apps] -Add: call a c function from c using awk's func call (demonstrate that awk and c functions should not make any difference)
	[example_apps] -Fix: hash_re won't init std pipes twice
	[example_apps] -Fix: custom vio test sets up pipes correctly and closes the hass from the app's end of the deal at the end
	[example_apps] -Add: better explanation of the c_func_call example and hook it in the all-test-chain
	[libmawk] -Fix: C function call from execute with new function call conventions
	[libmawk] -Fix: call_functionp increases sp before push else the stack pointer points to the wrong place
	[libmawk] -Fix: restore sp on function call failure
	[libmawk] -Fix: before closing stdio on awk side, close stdin on app side to make sure both ends can be closed
	[libmawk] -Fix: do not leak function arguments already pushed on stack if an awk function call fails due to bad arguments
	[libmawk] -Fix: allow c functions to be called using mawk_call_function*()
	[libmawk] -Fix: don't close stdin if it's not open
	[libmawk] -Fix: don't use strdup() for c function reg
	[libmawk] -Fix: don't close non-fifo stdin as fifo
	[libmawk] -Fix: priting uninitialized cell should work and should result in empty string
	[libmawk] -Add: wants_to_exit flag so that a libmawk caller knows a script wants to exit
	[libmawk] -Add: function call API with cell argc/argv
	[parser] -Fix: really reset the parser stack when it gets freed - used to segfault for two includes on the topmost level
	[parser] -Fix: allow excess newlines in the main context - this fixes a corner case around multiple includes
	[regression] -Add: test for bifunct call()
	[regression] -Add: test for acall() (dynamic call with array as argument)
	[regression] -Add: test for stack grow on deep recursion
	[regression] -Add: test program for awk function call corner cases
	[regression] -Add: libmawk corner case test base
	[regression] -Fix: libmawk func call test diag messages are more clear on what's wrong or good
	[rexp] -Fix: infinite loop bug (gawk test noloop1 and noloop2) - contributed by Thomas E. Dickey as a mawk fix
	[vio] -Fix: set eof flags on fifo creation (fixes a conditional jump on uninited var bug)
	[vio] -Fix: closing FIN closes the vio layer even if fin buffer had not been initialized
	[vio] -Fix: close vf throug FIN to make sure buffers are also freed
	[vio] -Fix: simpler common header with refco and who-knows-what in the future
	[vio] -Fix: initialize refco to 0
	[vm] -Change: avoid recursive calls to execute() on plain hardwired awk function calls
	[vm] -Change: wants_to_ret is execution state instead - need to remember whether we are running range1 or range2
	[vm] -Fix: leak in function return value stack handling
	[vm] -Add: new convention in bi_funct calling: return stack may be a directive for executing an user functioin (for bi_call and bi_acall)
	[vm] -Fix: mawk_bi_call() and bifunct acall() follow the new no-recurse calling convention
	[vm] -Add: new low level c->awk call functions trying to follow the no-recurse execute_() conventions
	[vm] -Move: execute API into new execute.h
	[vm] -Add: a separate execution exit status for exit()
	[vm] -Add: execution run limit and resume
	[vm] -Fix: bi_funct getline can indicate 'nomore' instead of eof
	[vm] -Fix: properly save and restore old_stack_base and old_sp on stack danger (stack growth)
	[vm] -Cleanup: remumber types (continous numbering)
	[vm] -Add: more stack-related cell types and make room for future non-string additions
	[vm] -Fix: compare() returns double so that nan-checks can be performed
	[vm] -Fix: error return with value from execute()
	[vm] -Fix: make MAWK->pow big enough and make it safe (need to be unsigned long for these values and for the bit ops)


libmawk 0.10.1 (released: 2014-07-11, r1056)
	[compiler] -Fix: zero-offset bug in jmp push (xref test crash)
	[build] -Add: make test should run the standard regression tests; do not run tests automatically on build
	[core] -Add: zfifo: generic fifo implemented using zmalloc/zfree
	[core] -Add: mawk_file_find() has a create flag that can disable creating a new file node - useful for forcing existing/open files
	[core] -Cleanup: move fin struct to fin.h
	[core] -Cleanup: rename FIN to mawk_input_t for naming consistency
	[core] -Fix: rewritten mawk_FINgets - cleaner code that is non-blocking-safe
	[core] -Fix: zrealloc() should not crash if old ptr is NULL
	[core] -Fix: safer vf close, close outputs only in uninit, when EXIT has finished for sure
	[vio] -Fix: input is always fd, output is always FILE *; remove fgets and stdin related API
	[vio] -Clenaup: cleaner ways recognizing "/dev/std*"
	[vio] -Add: support for /dev/fd/xxx (fdopen() wrapper)
	[vio] -Add: an extra layer to dispatch vio calls - multiple vio implementations can coexist
	[vio] -Add: virtualize vio init (non-per-file vio hooks); use MAWK->vio_init instead of hardwired vio_orig (so that vio_orig can be omitted)
	[vio] -Del: setbuf from API - this callback was a hack, now all done by the call that sets up stdio
	[vio] -Change: replace is_stdouts with a more generic mark_no_close hook
	[vio] -Fix: use zmalloc/zfree for the mawk_vio_t allocations
	[vio] -Add: a generic fifo implementation that also replaces the artificial FIN buf
	[regression] -Cleanup: remove test/ and convert all shell script based tests to make-based tests in regression/
	[tools] -Fix: find_globals uses the right dir
	[tools] -Fix: sym prefix validator won't complain for main()
	[API] -Add: file name validation hook
	[API] -Add: call to register a file by name:type and vio handle in the high level list of files; also sets up input buffering as needed
	[API] -Add: call to close the default input fifo (sending eof to the script)
	[API] -Add: convenient stdio-init call for multi-staged init
	[example_app] -Add: custom output pipe hack examples and update pipe in buffer fill examples with all possible corner cases
	[example_app] -Add: file name validation and vio hook examples


libmawk 0.10.0 (released: 2014-06-26, r937)
	Major changes in core, mostly for virtual arrays:
		[array] -Add: introduce new VM instructions for all array-element-write operations and split the lvalue rule of the grammar; besides being new instructions, they do the same in the execution switch as before
		[array] -Add: virtualize array operations
		[array] -Add: virtual, per instance ENVIRON[] that copies environ[] upon the first access and affects exec()
		[array] -Del: ST_ENV: ENVIRON[] is not a global special case anymore, just a builtin array with a different implementation
		[core] -Fix: replace "short int type" and a bunch of C_ macros for cell type with a proper enum
		[core] -Add: macro option for cell operators for speed
		[core] -Fix: set ERRNO in fin after a read error
		[core] -Cleanup: mawk_ prefix zmalloc, zfree, ZMALLOC, ZFREE and a lot of other constants and macros
		[init] -Add: a flag for indicating that it is ok for initialize_argv() to end up without a script
		[dump] -Fix: print the code of illegal instructions
		[dump] -Add: da_text: use blank lines to separate blocks: functions, BEGIN, MAIN, END
		[dump] -Change: da_text: after user functions, dump code in BEGIN-MAIN-END order - it is more intuitive this way
		[dump] -Add: text dump includes a summary of the global symbol table
		[dump] -Change: introduce -W dumpsym for dumping symbols independently of dumping code
		[da_bin] -Fix: properly save/load C_SNULL and C_SPACE (special split argument types) to/from binary dump
	API CHANGES affecting libmawk:
		[core] -Clenaup: rename STRING to mawk_string_t
		[core] -Clenaup: rename CELL to libmawk_cell_t
		[core] -Change: use an union instead of dval in CELL, to make room for other data types
		[core] -Add: new cell type C_ARR_REF (should be execute()-internal); an array reference by name and index (instead of pointer to a cell)
		[API] -move parsing the script from init stage1 to stage2 to give the host app a chance to inject things into the context before any code is parsed
		[API] -Change: get_var should return a read-only CELL - direct modification of the cell is a bad idea due to possible side effects required at write
		[API] -Fix: libmawk_print_cell shall be called with the context (mawk_state_t) as any other call in the API
		[API] -Change: new, easier-to-use c-func calling conventions and helpers
		[array] -Cleanup: rename ARRAY to mawk_array_t
	New libmawk API features:
		[API] -Add: helper functions to return the numeric value of a cell
		[API] -Add: an easy way to set a cell to empty
		[API] -Add: high level array set_at calls and scalar set calls

libmawk 0.9.7 (released: 2014-05-17, r732)
	Major code structure changes:
		[vio] -Split: file/pipe open/close operations to virtual IO (vio)
		[linux] -Add: initial linux kernel module effort (defunct yet)
		[array] -Add: virtualized array access (except for write) with callback functions
		[libmawk] -Split: lmawk-comp and lmawk-exec frames - most code is in common yet
		[libmawk] -Add: MAWK_NO_FORK for configuring against system() and fork()
	Code cleanup:
		[da_bin] -Cleanup: compiler warnings around da_bin (binary script save/load)
		[da_bin] -Split: da_bin and da_bin_helper: precompiled binary script load doesn't directly call read() but an user provided callback for virtualization
		[da_bin] -Fix: array creation on binary load
		[da_bin] -Fix: when resetting code base size for loading binary, use zmalloc for proper allocation size
		[build] -Fix: Makefile cleanup for portability
		[libmawk] -Fix: prefix NF, RS and other similar macro names with MAWK_ - they collide in the Linux kernel
		[test_3rd] -Add: scripts for running optional 3rd party tests from vendor/ (for the gawk set, without much configuration or filtering)
		[regex] -Change: error handling without setjmp/longjmp (return values and a bunch of checks)
		[regex] -Fix: regex lib uses zmalloc memory allocation to ensure all memory is free'd after a context is destroyed
		[regex] -Add: tiny test program to trigger almost all possible error conditions in the regex lib
		[regex] -Fix: move runtime states of the regex lib in the mawk struct to make it reentrant
		[regex] -Fix: bi_vars are part of the mawk struct because parallel scripts may have different values for the same variables or even modify them from script
	More portable doubles (and numeric):
		[numeric] -Add: use porty's math_wrap for log() to get rid of undeterministic fpe
		[numeric] -Add: use PM math protection copied from libporty
		[numeric] -Add: new builtin function isnan()
		[numeric] -Fix: virtualize strtod int strtonum: on a real int-only platform (e.g. Linux kernel) there won't be strtod at all
	Fixes of the core functionality:
		[libmawk] -Fix: make sure runtime error exit code takes over in final_error_code
		[bi_func] -Fix: substr for start indices less than 1 behave similar to how substr() handles length overruns (silently truncate) - gawk has the same behavior
		[bi_func] -Fix: gsub() patch for the ^ bug
		[io] -Fix: redirection conditions messed up in r349
		[libmawk] -Fix: disable -v only when MAWK_NO_EXEC is set, and don't even consider MAWK_NO_COMP
		[libmawk] -Fix: fin_exec should respect a MAWK exit request and not retry reading
		[libmawk] -Fix: if FIN can not be open (invalid file name, nonexisting file), don't segfault but return error

libmawk 0.9.6 (missed)

libmawk 0.9.5 (released: 2012-10-13, r527)
	[libmawk] -Add: memory usage accounting figures; -Wmaxmem=size sets maximum
	                memory allocation in bytes (optional suffixes: k and m)
	[libmawk] -Add: binary save/load functionality (-Wcompile and -b)
	                works on 32 bit systems only (will be fixed later)
	[libmawk] -Change: replace hardwired double references with generin numeric
	                   (C_NUM, num_t) - libmawk can be configured to use int or
	                   double as numeric format (./configure --numeric=)
	[libmawk] -Cleanup: (portability tweak) const correctness in mawk_reverse_find
	[libmawk] -Add: (portability tweak) _POSIX_SOURCE 
	[libmawk] -Fix: (portability tweak) explicit rules for rexp/*.o incuding
	                $(CC) command line for old fashioned make having the wrong
	                implicit rule
	[libmawk] -Cleanup: (portability tweak) k&r style function
	                    declarations/definitions removed from rexp lib
	[libmawk] -Fix: (portability tweak) missing prototypes to avoid implicit
	                declaration warning
	[libmawk] -Change: disable two zfree() calls that would free main code to
	                   avoid double frees
	[libmawk] -Add: compile with app clfags
	[libmawk] -Cleanup: move scconfig'd -D options to conf.h from Makefile.conf
	                    to make compiler command lines shorter
	[libmawk] -Cleanup: new vars.[ch] for collecting variable-related code
	[libmawk] -Fix: mawk_mm (aka free_later) mechanism memory handling erros
	                when realloc()'d
	[doc] -Add: explain design decisions behind our two gnu-dependencies
	[doc] -Add: portability table (per system: compiles out of the box, has
	            FPE problems, awklib test ran fine)

libmawk 0.9.4 (released: 2010-12-26, r392)
	[scconfig] -Add: detect pipe(2) and set NO_PIPE, replacing HAVE_REAL_PIPES
	[scconfig] -Add: detect size_t include file (removes the ifdef mess)
	[scconfig] -Add: require detection of cc/fpic (fixes compilation on amd64)
	[libmawk] -Cleanup: del PROTO() macro: libmawk requires at least C89 compiler
	[libmawk] -Cleanup: remove revision history and file name comments from *.[ch]
	           copyright notice above all mawk copyright notices
	[libmawk] -Cleanup: version to clearly indicate that this is lmawk
	[libmawk] -Cleanup: unified indentation in all .c and .h files
	[libmawk] -Cleanup: remove a lot of compiler warnings, fix const correctness
	[libmawk] -Cleanup: Makefile.dep generation rule; proper, up-to-date Makefile.dep
	[libmawk] -Del: remove FAKE_PIPES support (was only for DOS anyway)
	[libmawk] -Del: void * is a must, PTR should not fall back to char *
	[libmawk] -Del: cut out MSDOS support with all its "dreaded segment nonsense"
	[libmawk] -Fix: get bison to generate a reentrant parser
	[libmawk] -Fix: move static globals to mawk state struct
	[libmawk] -Fix: memory leaks


libmawk 0.9.3 (released: 2010-12-21, r307)
	[libmawk] -Fix: use -fPIC for compiling (helps on amd64)
	[libmawk] -Add: new builtin variable ERRNO with a different error code for each corner case in extension calls
	[libmawk] -Add: new built-in variable LIBPATH for library search path
		- skip loading a script if name started with '+' and the script was already loaded
		- if script file name starts with '/', assume it to be an absolute path and skip LIBPATH search
	[scconfig] -Add: scconfig configures/generates Makefiles of libmawk after autodetecting system features
	[scconfig] -Del: config-user, v7, atarist: scconfig should take care of all supported systems
	[scconfig] -Add: central ./configure and Makefile
	[libmawk] -Fix: order of running BEGINs with "include"
	[libmawk] -Fix: memory leaks around parser state
	[awklib] -Add: three awk libraries:
		- lib_rand for reproducible pseudo random numbers
		- lib_array for packing/unpacking/printing arrays
		- lib_ascii for converting characters to ASCII codes


libmawk 0.9.2 (released: 2010-12-12, r229)
	[libmawk] -Fix: invalid memory handling around cells
	[libmawk] -Fix: manual page dates and author and project/executable name
	[libmawk] -Add: dynamic awk function calls (call() and acall())
	[libmawk] -Add: dynamic awk variable value fetch (valueof())
	[libmawk] -Add: include

libmawk 0.9.1 (released: 2009-08-14, r198)
	[API] -Add: remember userdata when registering and calling back c functions
	[API] -Add: option to suppress undefined function calls
	[API] -Add: new call allowing awk function calls without varargs
	[API] -Change: split up libmawk_initialize in 3 stages (optional)
	[libmawk] -Del: autotools, keep last generated Makefile for hand editing
	[libmawk] -Del: config.h (merged in Makefile.conf.in)
	[libmawk] -Rename: mawk -> lmawk (the binary)
	[libmawk] -Change: print cell prints integers without .000000
	[testapp] -Move: testapp out from libmawk (new dir structure)
	[testapp] -Fix: stack handling bug
	[doc] -Add: manual pages for libmawk calls

libmawk 0.9.0 (released: 2009-07-22, r146)
	* Initial release, based on mawk 1.3.3
	* r3...r8 Separate libmawk_call() from execute(); allow undefined functions
	* r9...r42 globals to struct (reentrant)
	* r43...r92 rename non-static functions to have a mawk_ prefix
	* r92...r145 libmawk

