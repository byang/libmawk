include "../lib_hex.awk"
BEGIN {
	print lib_hex_str2int("23456780")
	print lib_hex_str2int("19aBcDeF")
	print lib_hex_str2int("0xaaaa")
	print lib_hex_str2int("aaaa")
}
