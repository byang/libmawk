include "../lib_array.awk"

BEGIN {
	v = split("one two three four five six seven eight", A, " ");
	print "Array:"
	lib_array_print(" A", A);

	str = lib_array_pack(A);
	A[10] = str

	str = lib_array_pack(A);

	lib_array_unpack(B, str);

	print "Pack/Unpack A:"
	lib_array_print(" B", B);


	print "Pack/Unpack A[10]:"
	lib_array_unpack(C, A[10]);
	lib_array_print(" C", C);
}
