include "../lib_rand.awk"
BEGIN {
	lib_rand_srand(Q1, 1234567)
	for (i=1; i<=10; i++)
		q1 += lib_rand_rand(Q1)

	print "q1 sum: " q1

	q1=0
	lib_rand_srand(Q1, 1234567)
	lib_rand_srand(Q2, 7654321)
	for (i=1; i<=10; i++) {
		q1 += lib_rand_rand(Q1)
		q2 += lib_rand_rand(Q2)
	}

	print "q1 sum: " q1
	print "q2 sum: " q2
}
