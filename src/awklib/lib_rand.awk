function lib_rand_version()
{
	return 1
}
# core code by nsz


# STATE is an array that stores all states of a random queue
# random queues generate pseudo random numbers independently of other queues

# set random seed on STATE
# if seed is not provided, a random number us choosen (use srand() before that)
function lib_rand_srand(STATE, seed) {
	if (seed == "")
		seed = rand()
	STATE[lib_rand_state] = lib_rand_mod_2_26(int(seed))
}

# get next random number from queue STATE
function lib_rand_rand(STATE) {
	STATE[lib_rand_state] = lib_rand_mod_2_26(54514969*STATE[lib_rand_state] + 21095981)
	return STATE[lib_rand_state]/67108864
}

# internal
function lib_rand_mod_2_26(n) {
	n -= int(n/67108864)*67108864
	return n
}
