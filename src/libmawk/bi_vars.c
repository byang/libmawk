
/********************************************
bi_vars.c

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/
#include "mawk.h"
#include "symtype.h"
#include "bi_vars.h"
#include "field.h"
#include "init.h"
#include "memory.h"
#include "num.h"
#include "cell.h"
#include "array_environ.h"

/* the order here must match the order in bi_vars.h */

static const char *bi_var_names[NUM_BI_VAR] = {
	"NR",
	"FNR",
	"ARGC",
	"FILENAME",
	"OFS",
	"ORS",
	"RLENGTH",
	"RSTART",
	"SUBSEP",
	"ERRNO",
	"LIBPATH",
};

/* mawk_insert the builtin vars in the mawk_hash table */
void mawk_bi_vars_init(mawk_state_t * MAWK)
{
	register int i;
	register SYMTAB *s;


	for (i = 0; i < NUM_BI_VAR; i++) {
		s = mawk_insert(MAWK, bi_var_names[i]);
		s->type = i <= 1 ? ST_NR : ST_VAR;
		s->stval.cp = MAWK->bi_vars + i;
		/* MAWK->bi_vars[i].type = 0 which is C_NOINIT */
	}

	mawk_environ_init(MAWK);

	/* set defaults */

	FILENAME->type = C_STRING;
	FILENAME->ptr = (PTR) mawk_new_STRING(MAWK, "");

	OFS->type = C_STRING;
	OFS->ptr = (PTR) mawk_new_STRING(MAWK, " ");

	ORS->type = C_STRING;
	ORS->ptr = (PTR) mawk_new_STRING(MAWK, "\n");

	SUBSEP->type = C_STRING;
	SUBSEP->ptr = (PTR) mawk_new_STRING(MAWK, "\034");

	NR->type = FNR->type = C_NUM;
	/* dval is already 0.0 */

	LIBPATH->type = C_STRING;
	LIBPATH->ptr = (PTR) mawk_new_STRING(MAWK, ";~/.libmawk;/usr/local/lib/libmawk;/usr/lib/libmawk");

}

#ifdef MAWK_MEM_PEDANTIC
void mawk_bi_vars_uninit(mawk_state_t * MAWK)
{
	int i;

	/* these ones should be cell-destroyed */
	mawk_delete(MAWK, "FILENAME", 1);
	mawk_delete(MAWK, "OFS", 1);
	mawk_delete(MAWK, "ORS", 1);
	mawk_delete(MAWK, "SUBSEP", 1);
	mawk_delete(MAWK, "LIBPATH", 1);

	for (i = 0; i < NUM_BI_VAR; i++)
		mawk_delete(MAWK, bi_var_names[i], 0);

	/* this was not initialized as a bi_var, but it really is after initialization */
	mawk_delete(MAWK, "ARGV", 1);

	mawk_environ_uninit(MAWK);
}
#endif
