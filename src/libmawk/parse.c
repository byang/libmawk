/* A Bison parser, made by GNU Bison 3.3.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2019 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.3.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         Mawk_parse
#define yylex           Mawk_lex
#define yyerror         Mawk_error
#define yydebug         Mawk_debug
#define yynerrs         Mawk_nerrs


/* First part of user prologue.  */
#line 86 "parse.y" /* yacc.c:337  */

#include <stdio.h>
#include "mawk.h"
#include "types.h"
#include "symtype.h"
#include "code.h"
#include "memory.h"
#include "bi_funct.h"
#include "bi_vars.h"
#include "jmp.h"
#include "field.h"
#include "files.h"
#include "scan.h"
#include "zmalloc.h"
#include "f2d.h"


#define  YYMAXDEPTH	200

void mawk_eat_nl(mawk_state_t * MAWK, YYSTYPE *lvalp);
static void  resize_fblock(mawk_state_t *, FBLOCK *);
static void  switch_code_to_main(mawk_state_t *);
static void  mawk_code_array(mawk_state_t *, SYMTAB *);
static void  mawk_code_call_id(mawk_state_t *, CA_REC *, SYMTAB *);
static void  field_A2I(mawk_state_t *MAWK);
static void  check_var(mawk_state_t *, SYMTAB *);
static void  check_array(mawk_state_t *, SYMTAB *);
static void  RE_as_arg(mawk_state_t *MAWK);

void mawk_parser_include(mawk_state_t *MAWK, void *str);

#define  mawk_code_address(x) \
do { \
	if (is_local(x)) \
		mawk_code2op(MAWK, L_PUSHA, (x)->offset) ;\
	else \
		code2(MAWK, _PUSHA, (x)->stval.cp); \
} while(0)

#define  CDP(x)  (mawk_code_base+(x))
/* WARNING: These CDP() calculations become invalid after calls
   that might change code_base.  Which are:  code2(), mawk_code2op(),
   code_jmp() and code_pop().
*/

/* this nonsense caters to MSDOS large model */
#define  CODE_FE_PUSHA()  mawk_code_ptr->ptr = (PTR) 0 ; code1(FE_PUSHA)


#line 126 "y.tab.c" /* yacc.c:337  */
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_MAWK_Y_TAB_H_INCLUDED
# define YY_MAWK_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int Mawk_debug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    UNEXPECTED = 258,
    BAD_DECIMAL = 259,
    NL = 260,
    SEMI_COLON = 261,
    LBRACE = 262,
    RBRACE = 263,
    LBOX = 264,
    RBOX = 265,
    COMMA = 266,
    IO_OUT = 267,
    ASSIGN = 268,
    ADD_ASG = 269,
    SUB_ASG = 270,
    MUL_ASG = 271,
    DIV_ASG = 272,
    MOD_ASG = 273,
    POW_ASG = 274,
    QMARK = 275,
    COLON = 276,
    OR = 277,
    AND = 278,
    IN = 279,
    MATCH = 280,
    EQ = 281,
    NEQ = 282,
    LT = 283,
    LTE = 284,
    GT = 285,
    GTE = 286,
    CAT = 287,
    GETLINE = 288,
    PLUS = 289,
    MINUS = 290,
    MUL = 291,
    DIV = 292,
    MOD = 293,
    NOT = 294,
    UMINUS = 295,
    IO_IN = 296,
    PIPE = 297,
    POW = 298,
    INC_or_DEC = 299,
    DOLLAR = 300,
    FIELD = 301,
    LPAREN = 302,
    RPAREN = 303,
    DOUBLE = 304,
    STRING_ = 305,
    RE = 306,
    ID = 307,
    D_ID = 308,
    FUNCT_ID = 309,
    C_FUNCT_ID = 310,
    BUILTIN = 311,
    LENGTH = 312,
    PRINT = 313,
    PRINTF = 314,
    SPLIT = 315,
    MATCH_FUNC = 316,
    SUB = 317,
    GSUB = 318,
    DO = 319,
    WHILE = 320,
    FOR = 321,
    BREAK = 322,
    CONTINUE = 323,
    IF = 324,
    ELSE = 325,
    DELETE = 326,
    BEGIN = 327,
    END = 328,
    EXIT = 329,
    NEXT = 330,
    RETURN = 331,
    FUNCTION = 332,
    INCLUDE = 333
  };
#endif
/* Tokens.  */
#define UNEXPECTED 258
#define BAD_DECIMAL 259
#define NL 260
#define SEMI_COLON 261
#define LBRACE 262
#define RBRACE 263
#define LBOX 264
#define RBOX 265
#define COMMA 266
#define IO_OUT 267
#define ASSIGN 268
#define ADD_ASG 269
#define SUB_ASG 270
#define MUL_ASG 271
#define DIV_ASG 272
#define MOD_ASG 273
#define POW_ASG 274
#define QMARK 275
#define COLON 276
#define OR 277
#define AND 278
#define IN 279
#define MATCH 280
#define EQ 281
#define NEQ 282
#define LT 283
#define LTE 284
#define GT 285
#define GTE 286
#define CAT 287
#define GETLINE 288
#define PLUS 289
#define MINUS 290
#define MUL 291
#define DIV 292
#define MOD 293
#define NOT 294
#define UMINUS 295
#define IO_IN 296
#define PIPE 297
#define POW 298
#define INC_or_DEC 299
#define DOLLAR 300
#define FIELD 301
#define LPAREN 302
#define RPAREN 303
#define DOUBLE 304
#define STRING_ 305
#define RE 306
#define ID 307
#define D_ID 308
#define FUNCT_ID 309
#define C_FUNCT_ID 310
#define BUILTIN 311
#define LENGTH 312
#define PRINT 313
#define PRINTF 314
#define SPLIT 315
#define MATCH_FUNC 316
#define SUB 317
#define GSUB 318
#define DO 319
#define WHILE 320
#define FOR 321
#define BREAK 322
#define CONTINUE 323
#define IF 324
#define ELSE 325
#define DELETE 326
#define BEGIN 327
#define END 328
#define EXIT 329
#define NEXT 330
#define RETURN 331
#define FUNCTION 332
#define INCLUDE 333

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 136 "parse.y" /* yacc.c:352  */

mawk_cell_t *cp ;
SYMTAB *stp ;
int  start ; /* code starting address as offset from code_base */
PF_CP  fp ;  /* ptr to a (print/printf) or (sub/gsub) function */
const BI_REC *bip ; /* ptr to info about a builtin */
FBLOCK  *fbp  ; /* ptr to a function block */
ARG2_REC *arg2p ;
CA_REC   *ca_p  ;
int   ival ;
PTR   ptr ;

#line 338 "y.tab.c" /* yacc.c:352  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int Mawk_parse (mawk_state_t *MAWK);

#endif /* !YY_MAWK_Y_TAB_H_INCLUDED  */



#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  103
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1147

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  79
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  60
/* YYNRULES -- Number of rules.  */
#define YYNRULES  186
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  360

#define YYUNDEFTOK  2
#define YYMAXUTOK   333

/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  ((unsigned) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   213,   213,   214,   217,   218,   219,   222,   227,   229,
     232,   231,   238,   237,   244,   243,   251,   267,   250,   280,
     282,   288,   289,   295,   296,   300,   301,   303,   305,   311,
     314,   317,   321,   329,   329,   332,   333,   334,   335,   336,
     337,   338,   339,   340,   341,   342,   343,   344,   345,   346,
     347,   348,   349,   350,   351,   352,   354,   383,   382,   390,
     389,   396,   397,   396,   402,   403,   407,   409,   411,   419,
     423,   427,   428,   429,   430,   431,   432,   433,   435,   437,
     439,   442,   450,   455,   462,   466,   473,   482,   483,   486,
     488,   493,   504,   514,   517,   526,   527,   530,   531,   535,
     539,   544,   548,   549,   556,   561,   565,   569,   577,   582,
     588,   608,   634,   658,   659,   663,   664,   681,   685,   698,
     703,   716,   729,   742,   754,   771,   779,   790,   804,   821,
     823,   832,   846,   848,   852,   856,   857,   858,   859,   860,
     861,   862,   868,   872,   879,   881,   905,   912,   935,   938,
     941,   944,   949,   956,   962,   967,   972,   979,   983,   983,
     983,   985,   989,   997,  1016,  1017,  1021,  1026,  1034,  1042,
    1061,  1084,  1091,  1092,  1095,  1101,  1114,  1126,  1137,  1148,
    1150,  1165,  1167,  1174,  1183,  1189,  1197
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "UNEXPECTED", "BAD_DECIMAL", "NL",
  "SEMI_COLON", "LBRACE", "RBRACE", "LBOX", "RBOX", "COMMA", "IO_OUT",
  "ASSIGN", "ADD_ASG", "SUB_ASG", "MUL_ASG", "DIV_ASG", "MOD_ASG",
  "POW_ASG", "QMARK", "COLON", "OR", "AND", "IN", "MATCH", "EQ", "NEQ",
  "LT", "LTE", "GT", "GTE", "CAT", "GETLINE", "PLUS", "MINUS", "MUL",
  "DIV", "MOD", "NOT", "UMINUS", "IO_IN", "PIPE", "POW", "INC_or_DEC",
  "DOLLAR", "FIELD", "LPAREN", "RPAREN", "DOUBLE", "STRING_", "RE", "ID",
  "D_ID", "FUNCT_ID", "C_FUNCT_ID", "BUILTIN", "LENGTH", "PRINT", "PRINTF",
  "SPLIT", "MATCH_FUNC", "SUB", "GSUB", "DO", "WHILE", "FOR", "BREAK",
  "CONTINUE", "IF", "ELSE", "DELETE", "BEGIN", "END", "EXIT", "NEXT",
  "RETURN", "FUNCTION", "INCLUDE", "$accept", "program", "program_block",
  "PA_block", "$@1", "$@2", "$@3", "$@4", "$@5", "block",
  "block_or_separator", "statement_list", "statement", "separator", "expr",
  "$@6", "$@7", "$@8", "$@9", "cat_expr", "p_expr", "lvalue", "arglist",
  "args", "builtin", "mark", "print", "pr_args", "arg2", "pr_direction",
  "if_front", "else", "do", "while_front", "for1", "for2", "for3",
  "bifunct_target_arr", "lvalue_arrwr", "array_loop_front", "field",
  "split_front", "split_back", "re_arg", "return_statement", "getline",
  "bifunct_target", "getline_file", "sub_or_gsub", "sub_back",
  "function_def", "funct_start", "funct_head", "f_arglist", "f_args",
  "outside_error", "call_args", "ca_front", "ca_back", "include", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333
};
# endif

#define YYPACT_NINF -225

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-225)))

#define YYTABLE_NINF -123

#define yytable_value_is_error(Yytable_value) \
  (!!((Yytable_value) == (-123)))

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     351,  -225,  -225,   486,  -225,   811,   811,   811,   104,   718,
    -225,   842,  -225,  -225,  -225,   219,  -225,  -225,  -225,  -225,
     -45,   -10,  -225,  -225,  -225,  -225,     7,    -9,   316,  -225,
    -225,  -225,  1074,   811,   153,  1038,  -225,  1073,    73,     1,
      32,   811,     4,  -225,    48,    11,    48,  -225,    30,  -225,
    -225,  -225,  -225,  -225,    16,    18,    25,    25,    22,    19,
     625,    25,   625,  -225,   414,  -225,  -225,   411,  -225,   558,
     558,   558,   656,   558,  -225,   842,    20,   120,    37,   120,
     120,    59,    67,  -225,  -225,  -225,    67,  -225,   475,     2,
     198,  -225,    74,    38,    38,    46,   842,   842,    48,    48,
    -225,  -225,  -225,  -225,  -225,  -225,  -225,  -225,  -225,    43,
     842,   842,   842,   842,   842,   842,   842,   169,   153,   811,
     811,   811,   811,   811,    66,   811,   842,   842,   842,   842,
     842,   842,   842,   842,   842,   842,   842,   842,   842,   842,
     842,   842,   842,   842,   842,   842,   842,  -225,   842,  -225,
    -225,  -225,    67,  -225,  -225,  -225,    60,   127,   842,  -225,
      71,  -225,  -225,  -225,   842,   687,  -225,  -225,   842,    25,
    -225,   411,  -225,  -225,   411,    25,  -225,  -225,  -225,   873,
      84,   101,  -225,  -225,  1041,   749,  -225,   948,   163,   130,
     184,   192,   842,  -225,   842,   203,  -225,   842,   171,  -225,
     904,  -225,   842,  1095,  1116,  -225,  -225,   842,   842,   842,
     842,  -225,   307,  -225,  -225,  -225,  -225,  -225,  -225,  -225,
    -225,  -225,   216,   216,   120,   120,   120,   249,   196,   559,
     559,   559,   559,   559,   559,   559,   559,   559,   559,   559,
     559,   559,   559,   559,   559,   559,   559,   559,   559,   559,
     960,   231,  -225,   559,   230,  -225,   197,   233,   975,  -225,
     259,  1053,   987,  -225,   238,  -225,  -225,   780,   559,  -225,
     237,   239,  -225,   558,   208,  -225,  -225,  1002,   558,   842,
     842,   842,   559,   559,   204,    34,  -225,     8,   547,  -225,
     202,   209,   842,   559,   487,   626,   260,  -225,  -225,   842,
     842,  -225,   210,  -225,   212,  -225,  -225,   842,  -225,     9,
     842,   842,    25,  -225,   842,  -225,  -225,   188,   193,   195,
    -225,   313,  -225,  -225,  -225,  -225,  -225,  -225,   217,   169,
    -225,   211,   617,  -225,   221,   214,   203,   559,   559,  -225,
    1014,   226,  -225,  -225,  -225,  -225,  -225,   842,  -225,   249,
    -225,  -225,  -225,    25,    25,   559,   223,  -225,  -225,  -225
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,   176,     9,     0,   157,     0,     0,     0,     0,     0,
     129,     0,    66,    67,    70,    68,    93,    93,    93,    92,
       0,     0,   164,   165,    10,    12,     0,     0,     0,     2,
       4,     7,    14,    35,    64,     0,    80,     0,   134,     0,
     152,     0,     0,     5,     0,     0,     0,     8,     0,    33,
      34,    95,    96,   108,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    25,     0,    23,    27,     0,    93,     0,
       0,     0,     0,     0,    31,     0,    68,    78,   134,    79,
      77,     0,    86,    82,    83,    85,   130,   132,     0,     0,
     134,    81,     0,     0,     0,     0,     0,     0,     0,     0,
     170,   171,   186,     1,     3,    16,    61,    57,    59,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    65,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    84,     0,   144,
     142,   161,    86,   158,   159,   160,   153,   154,     0,   168,
     172,     6,    20,    28,     0,     0,    29,    30,     0,    93,
     148,     0,    32,   150,     0,     0,    19,    24,    26,    87,
     105,     0,   111,   115,     0,     0,   128,     0,     0,     0,
       0,     0,     0,    69,     0,     0,   133,     0,   181,   177,
       0,   178,    87,     0,     0,    11,    13,     0,     0,     0,
       0,   119,    56,    50,    51,    52,    53,    54,    55,    21,
      15,    22,    71,    72,    73,    74,    75,   155,    76,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,   135,   136,   137,   138,   139,   140,   141,
       0,     0,   162,   147,     0,   174,     0,   173,     0,   113,
      68,     0,     0,   126,     0,   149,   151,     0,    89,    97,
      88,   102,   106,     0,     0,   116,   117,     0,     0,     0,
       0,     0,   100,   101,     0,     0,   179,    68,     0,   180,
       0,     0,     0,    17,     0,    58,    60,   156,   145,     0,
       0,   169,     0,   110,     0,   114,   104,     0,    99,     0,
       0,     0,     0,   107,     0,   118,   112,     0,     0,     0,
     120,   123,   183,   185,   182,   184,    91,   143,     0,     0,
      62,     0,     0,   175,     0,     0,    98,    90,   103,    94,
       0,   123,   122,   131,   124,   146,    18,     0,   121,     0,
     166,   163,   127,     0,     0,    63,     0,   125,   109,   167
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -225,  -225,   254,  -225,  -225,  -225,  -225,  -225,  -225,    54,
     -36,  -225,   -55,   -14,     0,  -225,  -225,  -225,  -225,  -225,
      61,    -7,    95,   -50,  -225,    91,  -225,  -225,    35,  -225,
    -225,  -225,  -225,  -225,  -225,  -225,  -225,  -225,   296,  -225,
      -1,  -225,  -225,    13,  -225,  -225,  -224,  -225,  -225,  -225,
    -225,  -225,  -225,  -225,  -225,  -225,   215,  -225,  -225,  -225
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    28,    29,    30,    98,    99,   117,   207,   329,    63,
     220,    64,    65,    66,    67,   209,   210,   208,   347,    33,
      34,    35,   269,   270,    36,    92,    68,   271,    89,   312,
      69,   273,    70,    71,    72,   185,   278,   154,    37,    73,
      38,    39,   150,   254,    74,    40,   156,    41,    42,   351,
      43,    44,    45,   256,   257,    46,   199,   200,   289,    47
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      32,    83,    96,   297,    78,    78,    78,    85,    78,   177,
      90,    88,   148,   194,   180,   181,   182,   -93,   186,   322,
     194,   -86,   -86,   -86,   -86,   -86,   -86,   -86,    32,   -93,
      49,    50,    78,   153,   163,    49,    50,    97,   162,   155,
      78,   102,   166,   167,   321,   310,   170,   172,   173,   149,
     195,   158,    91,   178,    31,     3,   323,   336,   160,   100,
     171,   101,   174,   164,    91,   165,    77,    79,    80,   168,
      87,   169,   184,   151,    90,   187,   -93,     9,    10,    81,
     189,   147,    31,   197,   152,   198,   140,   141,   142,   143,
     144,   145,   146,   202,   118,   211,   203,   204,   159,   227,
     161,   252,   157,   221,     9,    10,    81,    93,    94,    95,
     212,   213,   214,   215,   216,   217,   218,   147,    78,    78,
      78,    78,    78,   255,    78,   356,   229,   230,   231,   232,
     233,   234,   235,   236,   237,   238,   239,   240,   241,   242,
     243,   244,   245,   246,   247,   248,   249,   285,   250,     9,
      10,    81,   205,   206,   272,   263,    82,   265,   253,   179,
     266,   163,   124,   125,   258,   261,   274,   188,   262,  -123,
     125,   219,   279,   190,    49,    50,     3,   191,   196,   268,
     222,   223,   224,   225,   226,   277,   228,   119,   120,   121,
     122,   123,   282,   280,   283,   124,   125,   268,   341,   310,
     288,   281,   268,   342,   310,   343,   310,   293,   294,   295,
     296,   140,   141,   142,   143,   144,   145,   146,   313,   286,
     153,   348,   310,   316,   353,   310,   155,   284,   -93,   317,
     318,   319,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   125,
     299,   300,   147,   251,   302,   301,   196,   307,   310,   331,
     326,   311,   121,   122,   123,   314,   320,   335,   124,   125,
     264,   327,   333,    91,   334,   345,    90,    88,   -93,   352,
     344,   359,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   268,
     268,   268,   104,   304,   109,   110,   111,   112,   113,   114,
     115,   116,   253,   346,     9,    10,    81,   290,   339,   268,
     332,   152,   309,    91,    84,   328,     0,   268,     0,   201,
     337,   338,     0,     0,   340,   221,   103,     1,     0,     0,
       0,     2,     0,     3,     0,     0,  -122,  -122,  -122,  -122,
    -122,  -122,  -122,   111,   112,   113,   114,   115,   116,   357,
     358,     0,   153,     0,     0,     0,     0,   355,   155,     4,
       5,     6,     1,     0,     0,     7,     2,   344,     3,     0,
       8,     9,    10,    11,     0,    12,    13,    14,    15,     0,
      16,    17,    18,    19,     0,     0,    20,    21,    22,    23,
       0,     0,     0,   219,     4,     5,     6,     0,    24,    25,
       7,     0,     0,    26,    27,     8,     9,    10,    11,     0,
      12,    13,    14,    15,     0,    16,    17,    18,    19,     0,
       0,    20,    21,    22,    23,   175,    49,    50,     0,    49,
      50,     3,   176,    24,    25,     0,     0,     0,    26,    27,
       0,   106,     0,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,     0,     0,     0,     0,     4,     5,     6,
       0,     0,     0,     7,     0,     0,     0,     0,     8,     9,
      10,    11,     0,    12,    13,    14,    15,     0,    16,    17,
      18,    19,    51,    52,    20,    21,    22,    23,    53,    54,
      55,    56,    57,    58,     0,    59,   192,    48,    60,    61,
      62,    49,    50,     3,     0,   106,     0,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   106,   330,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,     4,
       5,     6,     0,   193,     0,     7,     0,     0,     0,     0,
       8,     9,    10,    11,     0,    12,    13,    14,    15,     0,
      16,    17,    18,    19,    51,    52,    20,    21,    22,    23,
      53,    54,    55,    56,    57,    58,     0,    59,   324,   175,
      60,    61,    62,    49,    50,     3,     0,   106,     0,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   106,
       0,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,     4,     5,     6,     0,   325,     0,     7,     0,     0,
       0,     0,     8,     9,    10,    11,     0,    12,    13,    14,
      15,     0,    16,    17,    18,    19,    51,    52,    20,    21,
      22,    23,    53,    54,    55,    56,    57,    58,   349,    59,
      49,    50,    60,    61,    62,     0,     0,   106,     0,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   108,
     109,   110,   111,   112,   113,   114,   115,   116,     4,     5,
       6,     0,   183,     0,     7,   350,     0,     0,     0,     8,
       9,    10,    11,     0,    12,    13,    14,    15,     0,    16,
      17,    18,    19,     0,     0,    20,    21,    22,    23,     4,
       5,     6,     0,   259,     0,     7,     0,     0,     0,     0,
       8,     9,    10,    11,     0,    12,    13,    14,    15,     0,
      16,    17,    18,    19,     0,     0,    20,    21,    22,    23,
       4,     5,     6,     0,     0,     0,     7,     0,     0,     0,
       0,     8,     9,    10,    11,     0,    12,    13,    14,   260,
       0,    16,    17,    18,    19,     0,     0,    20,    21,    22,
      23,     4,     5,     6,     0,     0,     0,     7,     0,     0,
       0,     0,     8,     9,    10,    75,     0,    12,    13,    14,
      76,    86,    16,    17,    18,    19,     0,     0,    20,    21,
      22,    23,     4,     5,     6,     0,     0,     0,     7,     0,
       0,     0,     0,     8,     9,    10,    11,   276,    12,    13,
      14,    15,     0,    16,    17,    18,    19,     0,     0,    20,
      21,    22,    23,     4,     5,     6,     0,     0,     0,     7,
       0,     0,     0,     0,     8,     9,    10,    11,   308,    12,
      13,    14,    15,     0,    16,    17,    18,    19,     0,     0,
      20,    21,    22,    23,     4,     5,     6,     0,     0,     0,
       7,     0,     0,     0,     0,     8,     9,    10,    75,     0,
      12,    13,    14,    76,     0,    16,    17,    18,    19,     0,
       0,    20,    21,    22,    23,     4,     5,     6,     0,     0,
       0,     7,     0,     0,     0,     0,     8,     9,    10,    11,
       0,    12,    13,    14,    15,     0,    16,    17,    18,    19,
       0,     0,    20,    21,    22,    23,     4,     5,     6,     0,
       0,     0,     7,     0,     0,     0,     0,     8,     9,    10,
     267,     0,    12,    13,    14,    15,     0,    16,    17,    18,
      19,     0,     0,    20,    21,    22,    23,     4,     5,     6,
       0,     0,     0,     7,     0,     0,     0,     0,     8,     9,
      10,    11,     0,    12,    13,    14,   287,     0,    16,    17,
      18,    19,     0,     0,    20,    21,    22,    23,   106,     0,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     106,     0,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,     0,     0,     0,   106,   193,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   106,   298,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,     0,
       0,     0,   106,   303,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   106,   306,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,     0,   275,     0,     0,
     315,   126,   127,   128,   129,   130,   131,   132,     0,   305,
       0,   106,   354,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   106,     0,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   105,   133,   134,   135,   136,
     137,   138,   139,     0,   106,     0,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   291,     0,     0,     0,
       0,     0,     0,     0,     0,   106,     0,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   292,     0,     0,
       0,     0,     0,     0,     0,     0,   106,     0,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116
};

static const yytype_int16 yycheck[] =
{
       0,     8,    47,   227,     5,     6,     7,     8,     9,    64,
      11,    11,    11,    11,    69,    70,    71,     9,    73,    11,
      11,    13,    14,    15,    16,    17,    18,    19,    28,     9,
       5,     6,    33,    40,    48,     5,     6,    47,     8,    40,
      41,    50,    56,    57,    10,    11,    60,    61,    62,    48,
      48,    47,    44,    67,     0,     7,    48,    48,    47,    52,
      60,    54,    62,    47,    44,    47,     5,     6,     7,    47,
       9,    52,    72,    41,    75,    75,     9,    45,    46,    47,
      81,    44,    28,     9,    52,    47,    13,    14,    15,    16,
      17,    18,    19,    47,    33,    52,    96,    97,    44,    33,
      46,    41,    41,   117,    45,    46,    47,    16,    17,    18,
     110,   111,   112,   113,   114,   115,   116,    44,   119,   120,
     121,   122,   123,    52,   125,   349,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   197,   148,    45,
      46,    47,    98,    99,    70,   169,    52,   171,   158,    68,
     174,   175,    42,    43,   164,   165,    65,    76,   168,    42,
      43,   117,     9,    82,     5,     6,     7,    86,    48,   179,
     119,   120,   121,   122,   123,   185,   125,    34,    35,    36,
      37,    38,   192,     9,   194,    42,    43,   197,    10,    11,
     200,     9,   202,    10,    11,    10,    11,   207,   208,   209,
     210,    13,    14,    15,    16,    17,    18,    19,   273,    48,
     227,    10,    11,   278,    10,    11,   227,    24,     9,   279,
     280,   281,    13,    14,    15,    16,    17,    18,    19,    43,
       9,    11,    44,   152,    11,    48,    48,     9,    11,   299,
      48,    12,    36,    37,    38,    47,    52,   307,    42,    43,
     169,    52,    52,    44,    52,    48,   267,   267,     9,    48,
      44,    48,    13,    14,    15,    16,    17,    18,    19,   279,
     280,   281,    28,    24,    24,    25,    26,    27,    28,    29,
      30,    31,   292,   329,    45,    46,    47,   202,   312,   299,
     300,    52,   267,    44,     8,   292,    -1,   307,    -1,    94,
     310,   311,    -1,    -1,   314,   329,     0,     1,    -1,    -1,
      -1,     5,    -1,     7,    -1,    -1,    13,    14,    15,    16,
      17,    18,    19,    26,    27,    28,    29,    30,    31,   353,
     354,    -1,   349,    -1,    -1,    -1,    -1,   347,   349,    33,
      34,    35,     1,    -1,    -1,    39,     5,    44,     7,    -1,
      44,    45,    46,    47,    -1,    49,    50,    51,    52,    -1,
      54,    55,    56,    57,    -1,    -1,    60,    61,    62,    63,
      -1,    -1,    -1,   329,    33,    34,    35,    -1,    72,    73,
      39,    -1,    -1,    77,    78,    44,    45,    46,    47,    -1,
      49,    50,    51,    52,    -1,    54,    55,    56,    57,    -1,
      -1,    60,    61,    62,    63,     1,     5,     6,    -1,     5,
       6,     7,     8,    72,    73,    -1,    -1,    -1,    77,    78,
      -1,    20,    -1,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    -1,    -1,    -1,    -1,    33,    34,    35,
      -1,    -1,    -1,    39,    -1,    -1,    -1,    -1,    44,    45,
      46,    47,    -1,    49,    50,    51,    52,    -1,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    -1,    71,    11,     1,    74,    75,
      76,     5,     6,     7,    -1,    20,    -1,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    33,
      34,    35,    -1,    48,    -1,    39,    -1,    -1,    -1,    -1,
      44,    45,    46,    47,    -1,    49,    50,    51,    52,    -1,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    -1,    71,    11,     1,
      74,    75,    76,     5,     6,     7,    -1,    20,    -1,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    20,
      -1,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    33,    34,    35,    -1,    48,    -1,    39,    -1,    -1,
      -1,    -1,    44,    45,    46,    47,    -1,    49,    50,    51,
      52,    -1,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    11,    71,
       5,     6,    74,    75,    76,    -1,    -1,    20,    -1,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    33,    34,
      35,    -1,     6,    -1,    39,    48,    -1,    -1,    -1,    44,
      45,    46,    47,    -1,    49,    50,    51,    52,    -1,    54,
      55,    56,    57,    -1,    -1,    60,    61,    62,    63,    33,
      34,    35,    -1,     6,    -1,    39,    -1,    -1,    -1,    -1,
      44,    45,    46,    47,    -1,    49,    50,    51,    52,    -1,
      54,    55,    56,    57,    -1,    -1,    60,    61,    62,    63,
      33,    34,    35,    -1,    -1,    -1,    39,    -1,    -1,    -1,
      -1,    44,    45,    46,    47,    -1,    49,    50,    51,    52,
      -1,    54,    55,    56,    57,    -1,    -1,    60,    61,    62,
      63,    33,    34,    35,    -1,    -1,    -1,    39,    -1,    -1,
      -1,    -1,    44,    45,    46,    47,    -1,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    -1,    -1,    60,    61,
      62,    63,    33,    34,    35,    -1,    -1,    -1,    39,    -1,
      -1,    -1,    -1,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    -1,    54,    55,    56,    57,    -1,    -1,    60,
      61,    62,    63,    33,    34,    35,    -1,    -1,    -1,    39,
      -1,    -1,    -1,    -1,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    -1,    54,    55,    56,    57,    -1,    -1,
      60,    61,    62,    63,    33,    34,    35,    -1,    -1,    -1,
      39,    -1,    -1,    -1,    -1,    44,    45,    46,    47,    -1,
      49,    50,    51,    52,    -1,    54,    55,    56,    57,    -1,
      -1,    60,    61,    62,    63,    33,    34,    35,    -1,    -1,
      -1,    39,    -1,    -1,    -1,    -1,    44,    45,    46,    47,
      -1,    49,    50,    51,    52,    -1,    54,    55,    56,    57,
      -1,    -1,    60,    61,    62,    63,    33,    34,    35,    -1,
      -1,    -1,    39,    -1,    -1,    -1,    -1,    44,    45,    46,
      47,    -1,    49,    50,    51,    52,    -1,    54,    55,    56,
      57,    -1,    -1,    60,    61,    62,    63,    33,    34,    35,
      -1,    -1,    -1,    39,    -1,    -1,    -1,    -1,    44,    45,
      46,    47,    -1,    49,    50,    51,    52,    -1,    54,    55,
      56,    57,    -1,    -1,    60,    61,    62,    63,    20,    -1,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      20,    -1,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    -1,    -1,    -1,    20,    48,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    20,    48,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    -1,
      -1,    -1,    20,    48,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    20,    48,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    -1,     6,    -1,    -1,
      48,    13,    14,    15,    16,    17,    18,    19,    -1,     6,
      -1,    20,    48,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    20,    -1,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    11,    13,    14,    15,    16,
      17,    18,    19,    -1,    20,    -1,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    11,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    11,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     1,     5,     7,    33,    34,    35,    39,    44,    45,
      46,    47,    49,    50,    51,    52,    54,    55,    56,    57,
      60,    61,    62,    63,    72,    73,    77,    78,    80,    81,
      82,    88,    93,    98,    99,   100,   103,   117,   119,   120,
     124,   126,   127,   129,   130,   131,   134,   138,     1,     5,
       6,    58,    59,    64,    65,    66,    67,    68,    69,    71,
      74,    75,    76,    88,    90,    91,    92,    93,   105,   109,
     111,   112,   113,   118,   123,    47,    52,    99,   119,    99,
      99,    47,    52,   100,   117,   119,    53,    99,    93,   107,
     119,    44,   104,   104,   104,   104,    47,    47,    83,    84,
      52,    54,    50,     0,    81,    11,    20,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    85,    99,    34,
      35,    36,    37,    38,    42,    43,    13,    14,    15,    16,
      17,    18,    19,    13,    14,    15,    16,    17,    18,    19,
      13,    14,    15,    16,    17,    18,    19,    44,    11,    48,
     121,    41,    52,   100,   116,   119,   125,    99,    47,    88,
      47,    88,     8,    92,    47,    47,    92,    92,    47,    52,
      92,    93,    92,    92,    93,     1,     8,    91,    92,   104,
      91,    91,    91,     6,    93,   114,    91,    93,   104,   119,
     104,   104,    11,    48,    11,    48,    48,     9,    47,   135,
     136,   135,    47,    93,    93,    88,    88,    86,    96,    94,
      95,    52,    93,    93,    93,    93,    93,    93,    93,    88,
      89,    92,    99,    99,    99,    99,    99,    33,    99,    93,
      93,    93,    93,    93,    93,    93,    93,    93,    93,    93,
      93,    93,    93,    93,    93,    93,    93,    93,    93,    93,
      93,   104,    41,    93,   122,    52,   132,   133,    93,     6,
      52,    93,    93,    92,   104,    92,    92,    47,    93,   101,
     102,   106,    70,   110,    65,     6,    48,    93,   115,     9,
       9,     9,    93,    93,    24,   102,    48,    52,    93,   137,
     101,    11,    11,    93,    93,    93,    93,   125,    48,     9,
      11,    48,    11,    48,    24,     6,    48,     9,    48,   107,
      11,    12,   108,    91,    47,    48,    91,   102,   102,   102,
      52,    10,    11,    48,    11,    48,    48,    52,   122,    87,
      21,   102,    93,    52,    52,   102,    48,    93,    93,    92,
      93,    10,    10,    10,    44,    48,    89,    97,    10,    11,
      48,   128,    48,    10,    48,    93,   125,    92,    92,    48
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    79,    80,    80,    81,    81,    81,    82,    82,    82,
      83,    82,    84,    82,    85,    82,    86,    87,    82,    88,
      88,    89,    89,    90,    90,    91,    91,    91,    91,    91,
      91,    91,    91,    92,    92,    93,    93,    93,    93,    93,
      93,    93,    93,    93,    93,    93,    93,    93,    93,    93,
      93,    93,    93,    93,    93,    93,    93,    94,    93,    95,
      93,    96,    97,    93,    98,    98,    99,    99,    99,    99,
      99,    99,    99,    99,    99,    99,    99,    99,    99,    99,
      99,    99,    99,    99,    99,    99,   100,   101,   101,   102,
     102,   103,   103,   104,    91,   105,   105,   106,   106,   106,
     107,   107,   108,   108,   109,    91,   110,    91,   111,    91,
     112,    91,    91,   113,   113,   114,   114,   115,   115,    93,
      93,   116,   117,    99,    99,    91,    91,   118,    91,   119,
     119,   119,   119,   119,    99,    93,    93,    93,    93,    93,
      93,    93,    99,   120,   121,   121,    99,   122,    91,    91,
     123,   123,    99,    99,    99,    99,    99,   124,   125,   125,
     125,   126,   126,    99,   127,   127,   128,   128,   129,   130,
     131,   131,   132,   132,   133,   133,   134,    99,    99,   135,
     135,   136,   136,   136,   137,   137,   138
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     1,     1,     2,     1,     1,     1,
       0,     3,     0,     3,     0,     3,     0,     0,     6,     3,
       3,     1,     1,     1,     2,     1,     2,     1,     2,     2,
       2,     1,     2,     1,     1,     1,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     0,     4,     0,
       4,     0,     0,     7,     1,     2,     1,     1,     1,     3,
       1,     3,     3,     3,     3,     3,     3,     2,     2,     2,
       1,     2,     2,     2,     2,     2,     1,     0,     1,     1,
       3,     5,     1,     0,     5,     1,     1,     1,     3,     2,
       3,     3,     0,     2,     4,     2,     1,     4,     1,     7,
       4,     2,     4,     3,     4,     1,     2,     1,     2,     3,
       5,     5,     5,     5,     6,     7,     3,     6,     2,     1,
       2,     6,     2,     3,     1,     3,     3,     3,     3,     3,
       3,     3,     2,     5,     1,     3,     6,     1,     2,     3,
       2,     3,     1,     2,     2,     3,     4,     1,     1,     1,
       1,     2,     3,     6,     1,     1,     1,     3,     2,     4,
       2,     2,     0,     1,     1,     3,     1,     3,     3,     2,
       2,     1,     3,     3,     2,     2,     2
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (MAWK, YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, MAWK); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep, mawk_state_t *MAWK)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  YYUSE (MAWK);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep, mawk_state_t *MAWK)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep, MAWK);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule, mawk_state_t *MAWK)
{
  unsigned long yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              , MAWK);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, MAWK); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return (YYSIZE_T) (yystpcpy (yyres, yystr) - yyres);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, mawk_state_t *MAWK)
{
  YYUSE (yyvaluep);
  YYUSE (MAWK);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (mawk_state_t *MAWK)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yynewstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  *yyssp = (yytype_int16) yystate;

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = (YYSIZE_T) (yyssp - yyss + 1);

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval, MAWK);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 7:
#line 223 "parse.y" /* yacc.c:1652  */
    { /* this do nothing action removes a vacuous warning
                  from Bison */
             }
#line 1856 "y.tab.c" /* yacc.c:1652  */
    break;

  case 10:
#line 232 "parse.y" /* yacc.c:1652  */
    { mawk_be_setup(MAWK, MAWK->scope = SCOPE_BEGIN) ; }
#line 1862 "y.tab.c" /* yacc.c:1652  */
    break;

  case 11:
#line 235 "parse.y" /* yacc.c:1652  */
    { switch_code_to_main(MAWK) ; }
#line 1868 "y.tab.c" /* yacc.c:1652  */
    break;

  case 12:
#line 238 "parse.y" /* yacc.c:1652  */
    { mawk_be_setup(MAWK, MAWK->scope = SCOPE_END) ; }
#line 1874 "y.tab.c" /* yacc.c:1652  */
    break;

  case 13:
#line 241 "parse.y" /* yacc.c:1652  */
    { switch_code_to_main(MAWK) ; }
#line 1880 "y.tab.c" /* yacc.c:1652  */
    break;

  case 14:
#line 244 "parse.y" /* yacc.c:1652  */
    { mawk_code_jmp(MAWK, _JZ, (INST*)0) ; }
#line 1886 "y.tab.c" /* yacc.c:1652  */
    break;

  case 15:
#line 247 "parse.y" /* yacc.c:1652  */
    { mawk_patch_jmp(MAWK, mawk_code_ptr ) ; }
#line 1892 "y.tab.c" /* yacc.c:1652  */
    break;

  case 16:
#line 251 "parse.y" /* yacc.c:1652  */
    { 
	       INST *p1 = CDP((yyvsp[-1].start)) ;
             int len ;

	       mawk_code_push(MAWK, p1, mawk_code_ptr - p1, MAWK->scope, MAWK->active_funct) ;
               mawk_code_ptr = p1 ;

               mawk_code2op(MAWK, _RANGE_CHK, 1) ;
               mawk_code_ptr += 3 ;
               len = mawk_code_pop(MAWK, mawk_code_ptr) ;
             mawk_code_ptr += len ;
               code1(_RANGE_STOP) ;
             p1 = CDP((yyvsp[-1].start)) ;
               p1[2].op = mawk_code_ptr - (p1+1) ;
             }
#line 1912 "y.tab.c" /* yacc.c:1652  */
    break;

  case 17:
#line 267 "parse.y" /* yacc.c:1652  */
    { code1(_RANGE_STOP) ; }
#line 1918 "y.tab.c" /* yacc.c:1652  */
    break;

  case 18:
#line 270 "parse.y" /* yacc.c:1652  */
    { 
	       INST *p1 = CDP((yyvsp[-5].start)) ;
	       
	       p1[3].op = CDP((yyvsp[0].start)) - (p1+1) ;
               p1[4].op = mawk_code_ptr - (p1+1) ;
             }
#line 1929 "y.tab.c" /* yacc.c:1652  */
    break;

  case 19:
#line 281 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ; }
#line 1935 "y.tab.c" /* yacc.c:1652  */
    break;

  case 20:
#line 283 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; /* does nothing won't be mawk_executed */
              MAWK->print_flag = MAWK->getline_flag = MAWK->paren_cnt = 0 ;
              yyerrok ; }
#line 1943 "y.tab.c" /* yacc.c:1652  */
    break;

  case 22:
#line 290 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ;
                       code1(_PUSHINT) ; code1(0) ;
                       code2(MAWK, _PRINT, mawk_f2d(mawk_bi_print)) ;
                     }
#line 1952 "y.tab.c" /* yacc.c:1652  */
    break;

  case 26:
#line 302 "parse.y" /* yacc.c:1652  */
    { code1(_POP) ; }
#line 1958 "y.tab.c" /* yacc.c:1652  */
    break;

  case 27:
#line 304 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; }
#line 1964 "y.tab.c" /* yacc.c:1652  */
    break;

  case 28:
#line 306 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ;
                MAWK->print_flag = MAWK->getline_flag = 0 ;
                MAWK->paren_cnt = 0 ;
                yyerrok ;
              }
#line 1974 "y.tab.c" /* yacc.c:1652  */
    break;

  case 29:
#line 312 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; mawk_BC_insert(MAWK, 'B', mawk_code_ptr+1) ;
               code2(MAWK, _JMP, 0) /* don't use mawk_code_jmp ! */ ; }
#line 1981 "y.tab.c" /* yacc.c:1652  */
    break;

  case 30:
#line 315 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; mawk_BC_insert(MAWK, 'C', mawk_code_ptr+1) ;
               code2(MAWK, _JMP, 0) ; }
#line 1988 "y.tab.c" /* yacc.c:1652  */
    break;

  case 31:
#line 318 "parse.y" /* yacc.c:1652  */
    { if ( MAWK->scope != SCOPE_FUNCT )
                     mawk_compile_error(MAWK, "return outside function body") ;
             }
#line 1996 "y.tab.c" /* yacc.c:1652  */
    break;

  case 32:
#line 322 "parse.y" /* yacc.c:1652  */
    { if ( MAWK->scope != SCOPE_MAIN )
                   mawk_compile_error(MAWK, "improper use of next" ) ;
                (yyval.start) = mawk_code_offset ; 
                code1(_NEXT) ;
              }
#line 2006 "y.tab.c" /* yacc.c:1652  */
    break;

  case 36:
#line 333 "parse.y" /* yacc.c:1652  */
    { code1(_ASSIGN) ; }
#line 2012 "y.tab.c" /* yacc.c:1652  */
    break;

  case 37:
#line 334 "parse.y" /* yacc.c:1652  */
    { code1(_ADD_ASG) ; }
#line 2018 "y.tab.c" /* yacc.c:1652  */
    break;

  case 38:
#line 335 "parse.y" /* yacc.c:1652  */
    { code1(_SUB_ASG) ; }
#line 2024 "y.tab.c" /* yacc.c:1652  */
    break;

  case 39:
#line 336 "parse.y" /* yacc.c:1652  */
    { code1(_MUL_ASG) ; }
#line 2030 "y.tab.c" /* yacc.c:1652  */
    break;

  case 40:
#line 337 "parse.y" /* yacc.c:1652  */
    { code1(_DIV_ASG) ; }
#line 2036 "y.tab.c" /* yacc.c:1652  */
    break;

  case 41:
#line 338 "parse.y" /* yacc.c:1652  */
    { code1(_MOD_ASG) ; }
#line 2042 "y.tab.c" /* yacc.c:1652  */
    break;

  case 42:
#line 339 "parse.y" /* yacc.c:1652  */
    { code1(_POW_ASG) ; }
#line 2048 "y.tab.c" /* yacc.c:1652  */
    break;

  case 43:
#line 340 "parse.y" /* yacc.c:1652  */
    { code1(_ASSIGN_ARR) ; }
#line 2054 "y.tab.c" /* yacc.c:1652  */
    break;

  case 44:
#line 341 "parse.y" /* yacc.c:1652  */
    { code1(_ADD_ASG_ARR) ; }
#line 2060 "y.tab.c" /* yacc.c:1652  */
    break;

  case 45:
#line 342 "parse.y" /* yacc.c:1652  */
    { code1(_SUB_ASG_ARR) ; }
#line 2066 "y.tab.c" /* yacc.c:1652  */
    break;

  case 46:
#line 343 "parse.y" /* yacc.c:1652  */
    { code1(_MUL_ASG_ARR) ; }
#line 2072 "y.tab.c" /* yacc.c:1652  */
    break;

  case 47:
#line 344 "parse.y" /* yacc.c:1652  */
    { code1(_DIV_ASG_ARR) ; }
#line 2078 "y.tab.c" /* yacc.c:1652  */
    break;

  case 48:
#line 345 "parse.y" /* yacc.c:1652  */
    { code1(_MOD_ASG_ARR) ; }
#line 2084 "y.tab.c" /* yacc.c:1652  */
    break;

  case 49:
#line 346 "parse.y" /* yacc.c:1652  */
    { code1(_POW_ASG_ARR) ; }
#line 2090 "y.tab.c" /* yacc.c:1652  */
    break;

  case 50:
#line 347 "parse.y" /* yacc.c:1652  */
    { code1(_EQ) ; }
#line 2096 "y.tab.c" /* yacc.c:1652  */
    break;

  case 51:
#line 348 "parse.y" /* yacc.c:1652  */
    { code1(_NEQ) ; }
#line 2102 "y.tab.c" /* yacc.c:1652  */
    break;

  case 52:
#line 349 "parse.y" /* yacc.c:1652  */
    { code1(_LT) ; }
#line 2108 "y.tab.c" /* yacc.c:1652  */
    break;

  case 53:
#line 350 "parse.y" /* yacc.c:1652  */
    { code1(_LTE) ; }
#line 2114 "y.tab.c" /* yacc.c:1652  */
    break;

  case 54:
#line 351 "parse.y" /* yacc.c:1652  */
    { code1(_GT) ; }
#line 2120 "y.tab.c" /* yacc.c:1652  */
    break;

  case 55:
#line 352 "parse.y" /* yacc.c:1652  */
    { code1(_GTE) ; }
#line 2126 "y.tab.c" /* yacc.c:1652  */
    break;

  case 56:
#line 355 "parse.y" /* yacc.c:1652  */
    {
	    INST *p3 = CDP((yyvsp[0].start)) ;

            if ( p3 == mawk_code_ptr - 2 )
            {
               if ( p3->op == _MATCH0 )  p3->op = _MATCH1 ;

               else /* check for string */
               if ( p3->op == _PUSHS )
               {
               	mawk_cell_t *cp = MAWK_ZMALLOC(MAWK, mawk_cell_t) ;

                 cp->type = C_STRING ;
                 cp->ptr = p3[1].ptr ;
                 mawk_cast_to_RE(MAWK, cp) ;
                 mawk_code_ptr -= 2 ;
                 code2(MAWK, _MATCH1, cp->ptr) ;
                 MAWK_ZFREE(MAWK, cp) ;
               }
               else  code1(_MATCH2) ;
            }
            else code1(_MATCH2) ;

            if ( !(yyvsp[-1].ival) ) code1(_NOT) ;
          }
#line 2156 "y.tab.c" /* yacc.c:1652  */
    break;

  case 57:
#line 383 "parse.y" /* yacc.c:1652  */
    { code1(_TEST) ;
                mawk_code_jmp(MAWK, _LJNZ, (INST*)0) ;
              }
#line 2164 "y.tab.c" /* yacc.c:1652  */
    break;

  case 58:
#line 387 "parse.y" /* yacc.c:1652  */
    { code1(_TEST) ; mawk_patch_jmp(MAWK, mawk_code_ptr) ; }
#line 2170 "y.tab.c" /* yacc.c:1652  */
    break;

  case 59:
#line 390 "parse.y" /* yacc.c:1652  */
    { code1(_TEST) ; 
		mawk_code_jmp(MAWK, _LJZ, (INST*)0) ;
	      }
#line 2178 "y.tab.c" /* yacc.c:1652  */
    break;

  case 60:
#line 394 "parse.y" /* yacc.c:1652  */
    { code1(_TEST) ; mawk_patch_jmp(MAWK, mawk_code_ptr) ; }
#line 2184 "y.tab.c" /* yacc.c:1652  */
    break;

  case 61:
#line 396 "parse.y" /* yacc.c:1652  */
    { mawk_code_jmp(MAWK, _JZ, (INST*)0) ; }
#line 2190 "y.tab.c" /* yacc.c:1652  */
    break;

  case 62:
#line 397 "parse.y" /* yacc.c:1652  */
    { mawk_code_jmp(MAWK, _JMP, (INST*)0) ; }
#line 2196 "y.tab.c" /* yacc.c:1652  */
    break;

  case 63:
#line 399 "parse.y" /* yacc.c:1652  */
    { mawk_patch_jmp(MAWK, mawk_code_ptr) ; mawk_patch_jmp(MAWK, CDP((yyvsp[0].start))) ; }
#line 2202 "y.tab.c" /* yacc.c:1652  */
    break;

  case 65:
#line 404 "parse.y" /* yacc.c:1652  */
    { code1(_CAT) ; }
#line 2208 "y.tab.c" /* yacc.c:1652  */
    break;

  case 66:
#line 408 "parse.y" /* yacc.c:1652  */
    {  (yyval.start) = mawk_code_offset ; code2(MAWK, _PUSHD, (yyvsp[0].ptr)) ; }
#line 2214 "y.tab.c" /* yacc.c:1652  */
    break;

  case 67:
#line 410 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; code2(MAWK, _PUSHS, (yyvsp[0].ptr)) ; }
#line 2220 "y.tab.c" /* yacc.c:1652  */
    break;

  case 68:
#line 412 "parse.y" /* yacc.c:1652  */
    { check_var(MAWK, (yyvsp[0].stp)) ;
            (yyval.start) = mawk_code_offset ;
            if ( is_local((yyvsp[0].stp)) )
            { mawk_code2op(MAWK, L_PUSHI, (yyvsp[0].stp)->offset) ; }
            else code2(MAWK, _PUSHI, (yyvsp[0].stp)->stval.cp) ;
          }
#line 2231 "y.tab.c" /* yacc.c:1652  */
    break;

  case 69:
#line 420 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ; }
#line 2237 "y.tab.c" /* yacc.c:1652  */
    break;

  case 70:
#line 424 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; code2(MAWK, _MATCH0, (yyvsp[0].ptr)) ; }
#line 2243 "y.tab.c" /* yacc.c:1652  */
    break;

  case 71:
#line 427 "parse.y" /* yacc.c:1652  */
    { code1(_ADD) ; }
#line 2249 "y.tab.c" /* yacc.c:1652  */
    break;

  case 72:
#line 428 "parse.y" /* yacc.c:1652  */
    { code1(_SUB) ; }
#line 2255 "y.tab.c" /* yacc.c:1652  */
    break;

  case 73:
#line 429 "parse.y" /* yacc.c:1652  */
    { code1(_MUL) ; }
#line 2261 "y.tab.c" /* yacc.c:1652  */
    break;

  case 74:
#line 430 "parse.y" /* yacc.c:1652  */
    { code1(_DIV) ; }
#line 2267 "y.tab.c" /* yacc.c:1652  */
    break;

  case 75:
#line 431 "parse.y" /* yacc.c:1652  */
    { code1(_MOD) ; }
#line 2273 "y.tab.c" /* yacc.c:1652  */
    break;

  case 76:
#line 432 "parse.y" /* yacc.c:1652  */
    { code1(_POW) ; }
#line 2279 "y.tab.c" /* yacc.c:1652  */
    break;

  case 77:
#line 434 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[0].start) ; code1(_NOT) ; }
#line 2285 "y.tab.c" /* yacc.c:1652  */
    break;

  case 78:
#line 436 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[0].start) ; code1(_UPLUS) ; }
#line 2291 "y.tab.c" /* yacc.c:1652  */
    break;

  case 79:
#line 438 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[0].start) ; code1(_UMINUS) ; }
#line 2297 "y.tab.c" /* yacc.c:1652  */
    break;

  case 81:
#line 443 "parse.y" /* yacc.c:1652  */
    { check_var(MAWK, (yyvsp[-1].stp)) ;
             (yyval.start) = mawk_code_offset ;
             mawk_code_address((yyvsp[-1].stp)) ;

             if ( (yyvsp[0].ival) == '+' )  code1(_POST_INC) ;
             else  code1(_POST_DEC) ;
           }
#line 2309 "y.tab.c" /* yacc.c:1652  */
    break;

  case 82:
#line 451 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[0].start) ; 
              if ( (yyvsp[-1].ival) == '+' ) code1(_PRE_INC) ;
              else  code1(_PRE_DEC) ;
            }
#line 2318 "y.tab.c" /* yacc.c:1652  */
    break;

  case 83:
#line 456 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[0].start) ; 
              if ( (yyvsp[-1].ival) == '+' ) code1(_PRE_INC_ARR) ;
              else  code1(_PRE_DEC_ARR) ;
            }
#line 2327 "y.tab.c" /* yacc.c:1652  */
    break;

  case 84:
#line 463 "parse.y" /* yacc.c:1652  */
    { if ((yyvsp[0].ival) == '+' ) code1(F_POST_INC ) ; 
             else  code1(F_POST_DEC) ;
           }
#line 2335 "y.tab.c" /* yacc.c:1652  */
    break;

  case 85:
#line 467 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[0].start) ; 
             if ( (yyvsp[-1].ival) == '+' ) code1(F_PRE_INC) ;
             else  code1( F_PRE_DEC) ; 
           }
#line 2344 "y.tab.c" /* yacc.c:1652  */
    break;

  case 86:
#line 474 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; 
          check_var(MAWK, (yyvsp[0].stp)) ;
          mawk_code_address((yyvsp[0].stp)) ;
        }
#line 2353 "y.tab.c" /* yacc.c:1652  */
    break;

  case 87:
#line 482 "parse.y" /* yacc.c:1652  */
    { (yyval.ival) = 0 ; }
#line 2359 "y.tab.c" /* yacc.c:1652  */
    break;

  case 89:
#line 487 "parse.y" /* yacc.c:1652  */
    { (yyval.ival) = 1 ; }
#line 2365 "y.tab.c" /* yacc.c:1652  */
    break;

  case 90:
#line 489 "parse.y" /* yacc.c:1652  */
    { (yyval.ival) = (yyvsp[-2].ival) + 1 ; }
#line 2371 "y.tab.c" /* yacc.c:1652  */
    break;

  case 91:
#line 494 "parse.y" /* yacc.c:1652  */
    { const BI_REC *p = (yyvsp[-4].bip) ;
          (yyval.start) = (yyvsp[-3].start) ;
          if ( (int)p->min_args > (yyvsp[-1].ival) || (int)p->max_args < (yyvsp[-1].ival) )
            mawk_compile_error(
            MAWK, "wrong number of arguments in call to %s" ,
            p->name ) ;
          if ( p->min_args != p->max_args ) /* variable args */
              { code1(_PUSHINT) ;  code1((yyvsp[-1].ival)) ; }
          code2(MAWK, _BUILTIN , mawk_f2d(p->fp)) ;
        }
#line 2386 "y.tab.c" /* yacc.c:1652  */
    break;

  case 92:
#line 505 "parse.y" /* yacc.c:1652  */
    {
	    (yyval.start) = mawk_code_offset ;
	    code1(_PUSHINT) ; code1(0) ;
	    code2(MAWK, _BUILTIN, mawk_f2d((yyvsp[0].bip)->fp)) ;
	  }
#line 2396 "y.tab.c" /* yacc.c:1652  */
    break;

  case 93:
#line 514 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; }
#line 2402 "y.tab.c" /* yacc.c:1652  */
    break;

  case 94:
#line 518 "parse.y" /* yacc.c:1652  */
    { code2(MAWK, _PRINT, mawk_f2d((yyvsp[-4].fp))) ;
              if ( (yyvsp[-4].fp) == mawk_bi_printf && (yyvsp[-2].ival) == 0 )
                    mawk_compile_error(MAWK, "no arguments in call to printf") ;
              MAWK->print_flag = 0 ;
              (yyval.start) = (yyvsp[-3].start) ;
            }
#line 2413 "y.tab.c" /* yacc.c:1652  */
    break;

  case 95:
#line 526 "parse.y" /* yacc.c:1652  */
    { (yyval.fp) = mawk_bi_print ; MAWK->print_flag = 1 ;}
#line 2419 "y.tab.c" /* yacc.c:1652  */
    break;

  case 96:
#line 527 "parse.y" /* yacc.c:1652  */
    { (yyval.fp) = mawk_bi_printf ; MAWK->print_flag = 1 ; }
#line 2425 "y.tab.c" /* yacc.c:1652  */
    break;

  case 97:
#line 530 "parse.y" /* yacc.c:1652  */
    { mawk_code2op(MAWK, _PUSHINT, (yyvsp[0].ival)) ; }
#line 2431 "y.tab.c" /* yacc.c:1652  */
    break;

  case 98:
#line 532 "parse.y" /* yacc.c:1652  */
    { (yyval.ival) = (yyvsp[-1].arg2p)->cnt ; mawk_zfree(MAWK, (yyvsp[-1].arg2p),sizeof(ARG2_REC)) ; 
             mawk_code2op(MAWK, _PUSHINT, (yyval.ival)) ; 
           }
#line 2439 "y.tab.c" /* yacc.c:1652  */
    break;

  case 99:
#line 536 "parse.y" /* yacc.c:1652  */
    { (yyval.ival)=0 ; mawk_code2op(MAWK, _PUSHINT, 0) ; }
#line 2445 "y.tab.c" /* yacc.c:1652  */
    break;

  case 100:
#line 540 "parse.y" /* yacc.c:1652  */
    { (yyval.arg2p) = (ARG2_REC*) mawk_zmalloc(MAWK, sizeof(ARG2_REC)) ;
             (yyval.arg2p)->start = (yyvsp[-2].start) ;
             (yyval.arg2p)->cnt = 2 ;
           }
#line 2454 "y.tab.c" /* yacc.c:1652  */
    break;

  case 101:
#line 545 "parse.y" /* yacc.c:1652  */
    { (yyval.arg2p) = (yyvsp[-2].arg2p) ; (yyval.arg2p)->cnt++ ; }
#line 2460 "y.tab.c" /* yacc.c:1652  */
    break;

  case 103:
#line 550 "parse.y" /* yacc.c:1652  */
    { mawk_code2op(MAWK, _PUSHINT, (yyvsp[-1].ival)) ; }
#line 2466 "y.tab.c" /* yacc.c:1652  */
    break;

  case 104:
#line 557 "parse.y" /* yacc.c:1652  */
    {  (yyval.start) = (yyvsp[-1].start) ; mawk_eat_nl(MAWK, &yylval) ; mawk_code_jmp(MAWK, _JZ, (INST*)0) ; }
#line 2472 "y.tab.c" /* yacc.c:1652  */
    break;

  case 105:
#line 562 "parse.y" /* yacc.c:1652  */
    { mawk_patch_jmp(MAWK, mawk_code_ptr ) ;  }
#line 2478 "y.tab.c" /* yacc.c:1652  */
    break;

  case 106:
#line 565 "parse.y" /* yacc.c:1652  */
    { mawk_eat_nl(MAWK, &yylval) ; mawk_code_jmp(MAWK, _JMP, (INST*)0) ; }
#line 2484 "y.tab.c" /* yacc.c:1652  */
    break;

  case 107:
#line 570 "parse.y" /* yacc.c:1652  */
    { mawk_patch_jmp(MAWK, mawk_code_ptr) ; 
		  mawk_patch_jmp(MAWK, CDP((yyvsp[0].start))) ; 
		}
#line 2492 "y.tab.c" /* yacc.c:1652  */
    break;

  case 108:
#line 578 "parse.y" /* yacc.c:1652  */
    { mawk_eat_nl(MAWK, &yylval) ; mawk_BC_new(MAWK) ; }
#line 2498 "y.tab.c" /* yacc.c:1652  */
    break;

  case 109:
#line 583 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-5].start) ;
          mawk_code_jmp(MAWK, _JNZ, CDP((yyvsp[-5].start))) ; 
          mawk_BC_clear(MAWK, mawk_code_ptr, CDP((yyvsp[-2].start))) ; }
#line 2506 "y.tab.c" /* yacc.c:1652  */
    break;

  case 110:
#line 589 "parse.y" /* yacc.c:1652  */
    { mawk_eat_nl(MAWK, &yylval) ; mawk_BC_new(MAWK) ;
                  (yyval.start) = (yyvsp[-1].start) ;

                  /* check if const expression */
                  if ( mawk_code_ptr - 2 == CDP((yyvsp[-1].start)) &&
                       mawk_code_ptr[-2].op == _PUSHD &&
                       *(double*)mawk_code_ptr[-1].ptr != 0.0 
                     )
                     mawk_code_ptr -= 2 ;
                  else
		  { INST *p3 = CDP((yyvsp[-1].start)) ;
		    mawk_code_push(MAWK, p3, mawk_code_ptr-p3, MAWK->scope, MAWK->active_funct) ;
		    mawk_code_ptr = p3 ;
                    code2(MAWK, _JMP, (INST*)0) ; /* code2() not mawk_code_jmp() */
		  }
                }
#line 2527 "y.tab.c" /* yacc.c:1652  */
    break;

  case 111:
#line 609 "parse.y" /* yacc.c:1652  */
    { 
		  int  saved_offset ;
		  int len ;
		  INST *p1 = CDP((yyvsp[-1].start)) ;
		  INST *p2 = CDP((yyvsp[0].start)) ;

                  if ( p1 != p2 )  /* real mawk_test in loop */
		  {
		    p1[1].op = mawk_code_ptr-(p1+1) ;
		    saved_offset = mawk_code_offset ;
		    len = mawk_code_pop(MAWK, mawk_code_ptr) ;
		    mawk_code_ptr += len ;
		    mawk_code_jmp(MAWK, _JNZ, CDP((yyvsp[0].start))) ;
		    mawk_BC_clear(MAWK, mawk_code_ptr, CDP(saved_offset)) ;
		  }
		  else /* while(1) */
		  {
		    mawk_code_jmp(MAWK, _JMP, p1) ;
		    mawk_BC_clear(MAWK, mawk_code_ptr, CDP((yyvsp[0].start))) ;
		  }
                }
#line 2553 "y.tab.c" /* yacc.c:1652  */
    break;

  case 112:
#line 635 "parse.y" /* yacc.c:1652  */
    { 
		  int cont_offset = mawk_code_offset ;
                  unsigned len = mawk_code_pop(MAWK, mawk_code_ptr) ;
		  INST *p2 = CDP((yyvsp[-2].start)) ;
		  INST *p4 = CDP((yyvsp[0].start)) ;

                  mawk_code_ptr += len ;

		  if ( p2 != p4 )  /* real mawk_test in for2 */
		  {
                    p4[-1].op = mawk_code_ptr - p4 + 1 ;
		    len = mawk_code_pop(MAWK, mawk_code_ptr) ;
		    mawk_code_ptr += len ;
                    mawk_code_jmp(MAWK, _JNZ, CDP((yyvsp[0].start))) ;
		  }
		  else /*  for(;;) */
		  mawk_code_jmp(MAWK, _JMP, p4) ;

		  mawk_BC_clear(MAWK, mawk_code_ptr, CDP(cont_offset)) ;

                }
#line 2579 "y.tab.c" /* yacc.c:1652  */
    break;

  case 113:
#line 658 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; }
#line 2585 "y.tab.c" /* yacc.c:1652  */
    break;

  case 114:
#line 660 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ; code1(_POP) ; }
#line 2591 "y.tab.c" /* yacc.c:1652  */
    break;

  case 115:
#line 663 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; }
#line 2597 "y.tab.c" /* yacc.c:1652  */
    break;

  case 116:
#line 665 "parse.y" /* yacc.c:1652  */
    { 
             if ( mawk_code_ptr - 2 == CDP((yyvsp[-1].start)) &&
                  mawk_code_ptr[-2].op == _PUSHD &&
                  * (double*) mawk_code_ptr[-1].ptr != 0.0
                )
                    mawk_code_ptr -= 2 ;
             else   
	     {
	       INST *p1 = CDP((yyvsp[-1].start)) ;
	       mawk_code_push(MAWK, p1, mawk_code_ptr-p1, MAWK->scope, MAWK->active_funct) ;
	       mawk_code_ptr = p1 ;
	       code2(MAWK, _JMP, (INST*)0) ;
	     }
           }
#line 2616 "y.tab.c" /* yacc.c:1652  */
    break;

  case 117:
#line 682 "parse.y" /* yacc.c:1652  */
    { mawk_eat_nl(MAWK, &yylval) ; mawk_BC_new(MAWK) ;
	     mawk_code_push(MAWK, (INST*)0,0, MAWK->scope, MAWK->active_funct) ;
	   }
#line 2624 "y.tab.c" /* yacc.c:1652  */
    break;

  case 118:
#line 686 "parse.y" /* yacc.c:1652  */
    { INST *p1 = CDP((yyvsp[-1].start)) ;

	     mawk_eat_nl(MAWK, &yylval) ; mawk_BC_new(MAWK) ; 
             code1(_POP) ;
             mawk_code_push(MAWK, p1, mawk_code_ptr - p1, MAWK->scope, MAWK->active_funct) ;
             mawk_code_ptr -= mawk_code_ptr - p1 ;
           }
#line 2636 "y.tab.c" /* yacc.c:1652  */
    break;

  case 119:
#line 699 "parse.y" /* yacc.c:1652  */
    { check_array(MAWK, (yyvsp[0].stp)) ;
             mawk_code_array(MAWK, (yyvsp[0].stp)) ; 
             code1(A_TEST) ; 
            }
#line 2645 "y.tab.c" /* yacc.c:1652  */
    break;

  case 120:
#line 704 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-3].arg2p)->start ;
             mawk_code2op(MAWK, A_CAT, (yyvsp[-3].arg2p)->cnt) ;
             mawk_zfree(MAWK, (yyvsp[-3].arg2p), sizeof(ARG2_REC)) ;

             check_array(MAWK, (yyvsp[0].stp)) ;
             mawk_code_array(MAWK, (yyvsp[0].stp)) ;
             code1(A_TEST) ;
           }
#line 2658 "y.tab.c" /* yacc.c:1652  */
    break;

  case 121:
#line 717 "parse.y" /* yacc.c:1652  */
    { 
             if ( (yyvsp[-1].ival) > 1 )
             { mawk_code2op(MAWK, A_CAT, (yyvsp[-1].ival)) ; }

             check_array(MAWK, (yyvsp[-4].stp)) ;
             if( is_local((yyvsp[-4].stp)) )
             { mawk_code2op(MAWK, LAE_PUSHA, (yyvsp[-4].stp)->offset) ; }
             else code2(MAWK, AE_PUSHA, (yyvsp[-4].stp)->stval.array) ;
             (yyval.start) = (yyvsp[-3].start) ;
           }
#line 2673 "y.tab.c" /* yacc.c:1652  */
    break;

  case 122:
#line 730 "parse.y" /* yacc.c:1652  */
    { 
             if ( (yyvsp[-1].ival) > 1 )
             { mawk_code2op(MAWK, A_CAT, (yyvsp[-1].ival)) ; }

             check_array(MAWK, (yyvsp[-4].stp)) ;
             if( is_local((yyvsp[-4].stp)) )
             { mawk_code2op(MAWK, LAE_PUSHA_WRARR, (yyvsp[-4].stp)->offset) ; }
             else code2(MAWK, AE_PUSHA_WRARR, (yyvsp[-4].stp)->stval.array) ;
             (yyval.start) = (yyvsp[-3].start) ;
           }
#line 2688 "y.tab.c" /* yacc.c:1652  */
    break;

  case 123:
#line 743 "parse.y" /* yacc.c:1652  */
    { 
             if ( (yyvsp[-1].ival) > 1 )
             { mawk_code2op(MAWK, A_CAT, (yyvsp[-1].ival)) ; }

             check_array(MAWK, (yyvsp[-4].stp)) ;
             if( is_local((yyvsp[-4].stp)) )
             { mawk_code2op(MAWK, LAE_PUSHI, (yyvsp[-4].stp)->offset) ; }
             else code2(MAWK, AE_PUSHI, (yyvsp[-4].stp)->stval.array) ;
             (yyval.start) = (yyvsp[-3].start) ;
           }
#line 2703 "y.tab.c" /* yacc.c:1652  */
    break;

  case 124:
#line 755 "parse.y" /* yacc.c:1652  */
    { 
             if ( (yyvsp[-2].ival) > 1 )
             { mawk_code2op(MAWK, A_CAT,(yyvsp[-2].ival)) ; }

             check_array(MAWK, (yyvsp[-5].stp)) ;
             if( is_local((yyvsp[-5].stp)) )
             { mawk_code2op(MAWK, LAE_PUSHA_WRARR, (yyvsp[-5].stp)->offset) ; }
             else code2(MAWK, AE_PUSHA_WRARR, (yyvsp[-5].stp)->stval.array) ;
             if ( (yyvsp[0].ival) == '+' )  code1(_POST_INC_ARR) ;
             else  code1(_POST_DEC_ARR) ;

             (yyval.start) = (yyvsp[-4].start) ;
           }
#line 2721 "y.tab.c" /* yacc.c:1652  */
    break;

  case 125:
#line 772 "parse.y" /* yacc.c:1652  */
    { 
               (yyval.start) = (yyvsp[-4].start) ;
               if ( (yyvsp[-2].ival) > 1 ) { mawk_code2op(MAWK, A_CAT, (yyvsp[-2].ival)) ; }
               check_array(MAWK, (yyvsp[-5].stp)) ;
               mawk_code_array(MAWK, (yyvsp[-5].stp)) ;
               code1(A_DEL) ;
             }
#line 2733 "y.tab.c" /* yacc.c:1652  */
    break;

  case 126:
#line 780 "parse.y" /* yacc.c:1652  */
    {
		(yyval.start) = mawk_code_offset ;
		check_array(MAWK, (yyvsp[-1].stp)) ;
		mawk_code_array(MAWK, (yyvsp[-1].stp)) ;
		code1(DEL_A) ;
	     }
#line 2744 "y.tab.c" /* yacc.c:1652  */
    break;

  case 127:
#line 791 "parse.y" /* yacc.c:1652  */
    { mawk_eat_nl(MAWK, &yylval) ; mawk_BC_new(MAWK) ;
                      (yyval.start) = mawk_code_offset ;

                      check_var(MAWK, (yyvsp[-3].stp)) ;
                      mawk_code_address((yyvsp[-3].stp)) ;
                      check_array(MAWK, (yyvsp[-1].stp)) ;
                      mawk_code_array(MAWK, (yyvsp[-1].stp)) ;

                      code2(MAWK, SET_ALOOP, (INST*)0) ;
                    }
#line 2759 "y.tab.c" /* yacc.c:1652  */
    break;

  case 128:
#line 805 "parse.y" /* yacc.c:1652  */
    { 
		INST *p2 = CDP((yyvsp[0].start)) ;

	        p2[-1].op = mawk_code_ptr - p2 + 1 ;
                mawk_BC_clear(MAWK, mawk_code_ptr+2 , mawk_code_ptr) ;
		mawk_code_jmp(MAWK, ALOOP, p2) ;
		code1(POP_AL) ;
              }
#line 2772 "y.tab.c" /* yacc.c:1652  */
    break;

  case 129:
#line 822 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ; code2(MAWK, F_PUSHA, (yyvsp[0].cp)) ; }
#line 2778 "y.tab.c" /* yacc.c:1652  */
    break;

  case 130:
#line 824 "parse.y" /* yacc.c:1652  */
    { check_var(MAWK, (yyvsp[0].stp)) ;
             (yyval.start) = mawk_code_offset ;
             if ( is_local((yyvsp[0].stp)) )
             { mawk_code2op(MAWK, L_PUSHI, (yyvsp[0].stp)->offset) ; }
             else code2(MAWK, _PUSHI, (yyvsp[0].stp)->stval.cp) ;

	     CODE_FE_PUSHA() ;
           }
#line 2791 "y.tab.c" /* yacc.c:1652  */
    break;

  case 131:
#line 833 "parse.y" /* yacc.c:1652  */
    { 
             if ( (yyvsp[-1].ival) > 1 )
             { mawk_code2op(MAWK, A_CAT, (yyvsp[-1].ival)) ; }

             check_array(MAWK, (yyvsp[-4].stp)) ;
             if( is_local((yyvsp[-4].stp)) )
             { mawk_code2op(MAWK, LAE_PUSHI, (yyvsp[-4].stp)->offset) ; }
             else code2(MAWK, AE_PUSHI, (yyvsp[-4].stp)->stval.array) ;

	     CODE_FE_PUSHA()  ;

             (yyval.start) = (yyvsp[-3].start) ;
           }
#line 2809 "y.tab.c" /* yacc.c:1652  */
    break;

  case 132:
#line 847 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[0].start) ;  CODE_FE_PUSHA() ; }
#line 2815 "y.tab.c" /* yacc.c:1652  */
    break;

  case 133:
#line 849 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ; }
#line 2821 "y.tab.c" /* yacc.c:1652  */
    break;

  case 134:
#line 853 "parse.y" /* yacc.c:1652  */
    { field_A2I(MAWK) ; }
#line 2827 "y.tab.c" /* yacc.c:1652  */
    break;

  case 135:
#line 856 "parse.y" /* yacc.c:1652  */
    { code1(F_ASSIGN) ; }
#line 2833 "y.tab.c" /* yacc.c:1652  */
    break;

  case 136:
#line 857 "parse.y" /* yacc.c:1652  */
    { code1(F_ADD_ASG) ; }
#line 2839 "y.tab.c" /* yacc.c:1652  */
    break;

  case 137:
#line 858 "parse.y" /* yacc.c:1652  */
    { code1(F_SUB_ASG) ; }
#line 2845 "y.tab.c" /* yacc.c:1652  */
    break;

  case 138:
#line 859 "parse.y" /* yacc.c:1652  */
    { code1(F_MUL_ASG) ; }
#line 2851 "y.tab.c" /* yacc.c:1652  */
    break;

  case 139:
#line 860 "parse.y" /* yacc.c:1652  */
    { code1(F_DIV_ASG) ; }
#line 2857 "y.tab.c" /* yacc.c:1652  */
    break;

  case 140:
#line 861 "parse.y" /* yacc.c:1652  */
    { code1(F_MOD_ASG) ; }
#line 2863 "y.tab.c" /* yacc.c:1652  */
    break;

  case 141:
#line 862 "parse.y" /* yacc.c:1652  */
    { code1(F_POW_ASG) ; }
#line 2869 "y.tab.c" /* yacc.c:1652  */
    break;

  case 142:
#line 869 "parse.y" /* yacc.c:1652  */
    { code2(MAWK, _BUILTIN, mawk_f2d(mawk_bi_split)) ; }
#line 2875 "y.tab.c" /* yacc.c:1652  */
    break;

  case 143:
#line 873 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-2].start) ;
              check_array(MAWK, (yyvsp[0].stp)) ;
              mawk_code_array(MAWK, (yyvsp[0].stp))  ;
            }
#line 2884 "y.tab.c" /* yacc.c:1652  */
    break;

  case 144:
#line 880 "parse.y" /* yacc.c:1652  */
    { code2(MAWK, _PUSHI, &MAWK->fs_shadow) ; }
#line 2890 "y.tab.c" /* yacc.c:1652  */
    break;

  case 145:
#line 882 "parse.y" /* yacc.c:1652  */
    { 
                  if ( CDP((yyvsp[-1].start)) == mawk_code_ptr - 2 )
                  {
                    if ( mawk_code_ptr[-2].op == _MATCH0 )
                        RE_as_arg(MAWK) ;
                    else
                    if ( mawk_code_ptr[-2].op == _PUSHS )
                    { mawk_cell_t *cp = MAWK_ZMALLOC(MAWK, mawk_cell_t) ;

                      cp->type = C_STRING ;
                      cp->ptr = mawk_code_ptr[-1].ptr ;
                      mawk_cast_for_split(MAWK, cp) ;
                      mawk_code_ptr[-2].op = _PUSHC ;
                      mawk_code_ptr[-1].ptr = (PTR) cp ;
                    }
                  }
                }
#line 2912 "y.tab.c" /* yacc.c:1652  */
    break;

  case 146:
#line 906 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-3].start) ; 
          code2(MAWK, _BUILTIN, mawk_f2d(mawk_bi_match)) ;
        }
#line 2920 "y.tab.c" /* yacc.c:1652  */
    break;

  case 147:
#line 913 "parse.y" /* yacc.c:1652  */
    {
	       INST *p1 = CDP((yyvsp[0].start)) ;

               if ( p1 == mawk_code_ptr - 2 ) 
               {
                 if ( p1->op == _MATCH0 ) RE_as_arg(MAWK) ;
                 else
                 if ( p1->op == _PUSHS )
                 { mawk_cell_t *cp = MAWK_ZMALLOC(MAWK, mawk_cell_t) ;

                   cp->type = C_STRING ;
                   cp->ptr = p1[1].ptr ;
                   mawk_cast_to_RE(MAWK, cp) ;
                   p1->op = _PUSHC ;
                   p1[1].ptr = (PTR) cp ;
                 } 
               }
             }
#line 2943 "y.tab.c" /* yacc.c:1652  */
    break;

  case 148:
#line 936 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ;
                      code1(_EXIT0) ; }
#line 2950 "y.tab.c" /* yacc.c:1652  */
    break;

  case 149:
#line 939 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ; code1(_EXIT) ; }
#line 2956 "y.tab.c" /* yacc.c:1652  */
    break;

  case 150:
#line 942 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ;
                      code1(_RET0) ; }
#line 2963 "y.tab.c" /* yacc.c:1652  */
    break;

  case 151:
#line 945 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ; code1(_RET) ; }
#line 2969 "y.tab.c" /* yacc.c:1652  */
    break;

  case 152:
#line 950 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ;
            code2(MAWK, F_PUSHA, &MAWK->field[0]) ;
            code1(_PUSHINT) ; code1(0) ; 
            code2(MAWK, _BUILTIN, mawk_f2d(mawk_bi_getline)) ;
            MAWK->getline_flag = 0 ;
          }
#line 2980 "y.tab.c" /* yacc.c:1652  */
    break;

  case 153:
#line 957 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[0].start) ;
            code1(_PUSHINT) ; code1(0) ;
            code2(MAWK, _BUILTIN, mawk_f2d(mawk_bi_getline)) ;
            MAWK->getline_flag = 0 ;
          }
#line 2990 "y.tab.c" /* yacc.c:1652  */
    break;

  case 154:
#line 963 "parse.y" /* yacc.c:1652  */
    { code1(_PUSHINT) ; code1(F_IN) ;
            code2(MAWK, _BUILTIN, mawk_f2d(mawk_bi_getline)) ;
            /* getline_flag already off in yylex() */
          }
#line 2999 "y.tab.c" /* yacc.c:1652  */
    break;

  case 155:
#line 968 "parse.y" /* yacc.c:1652  */
    { code2(MAWK, F_PUSHA, &MAWK->field[0]) ;
            code1(_PUSHINT) ; code1(PIPE_IN) ;
            code2(MAWK, _BUILTIN, mawk_f2d(mawk_bi_getline)) ;
          }
#line 3008 "y.tab.c" /* yacc.c:1652  */
    break;

  case 156:
#line 973 "parse.y" /* yacc.c:1652  */
    { 
            code1(_PUSHINT) ; code1(PIPE_IN) ;
            code2(MAWK, _BUILTIN, mawk_f2d(mawk_bi_getline)) ;
          }
#line 3017 "y.tab.c" /* yacc.c:1652  */
    break;

  case 157:
#line 979 "parse.y" /* yacc.c:1652  */
    { MAWK->getline_flag = 1 ; }
#line 3023 "y.tab.c" /* yacc.c:1652  */
    break;

  case 161:
#line 986 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ;
                   code2(MAWK, F_PUSHA, MAWK->field+0) ;
                 }
#line 3031 "y.tab.c" /* yacc.c:1652  */
    break;

  case 162:
#line 990 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ; }
#line 3037 "y.tab.c" /* yacc.c:1652  */
    break;

  case 163:
#line 998 "parse.y" /* yacc.c:1652  */
    {
	     INST *p5 = CDP((yyvsp[-1].start)) ;
	     INST *p6 = CDP((yyvsp[0].start)) ;

             if ( p6 - p5 == 2 && p5->op == _PUSHS  )
             { /* cast from STRING to REPL at compile time */
               mawk_cell_t *cp = MAWK_ZMALLOC(MAWK, mawk_cell_t) ;
               cp->type = C_STRING ;
               cp->ptr = p5[1].ptr ;
               mawk_cast_to_REPL(MAWK, cp) ;
               p5->op = _PUSHC ;
               p5[1].ptr = (PTR) cp ;
             }
             code2(MAWK, _BUILTIN, mawk_f2d((yyvsp[-5].fp))) ;
             (yyval.start) = (yyvsp[-3].start) ;
           }
#line 3058 "y.tab.c" /* yacc.c:1652  */
    break;

  case 164:
#line 1016 "parse.y" /* yacc.c:1652  */
    { (yyval.fp) = mawk_bi_sub ; }
#line 3064 "y.tab.c" /* yacc.c:1652  */
    break;

  case 165:
#line 1017 "parse.y" /* yacc.c:1652  */
    { (yyval.fp) = mawk_bi_gsub ; }
#line 3070 "y.tab.c" /* yacc.c:1652  */
    break;

  case 166:
#line 1022 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = mawk_code_offset ;
                  code2(MAWK, F_PUSHA, &MAWK->field[0]) ; 
                }
#line 3078 "y.tab.c" /* yacc.c:1652  */
    break;

  case 167:
#line 1027 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ; }
#line 3084 "y.tab.c" /* yacc.c:1652  */
    break;

  case 168:
#line 1035 "parse.y" /* yacc.c:1652  */
    { 
		   resize_fblock(MAWK, (yyvsp[-1].fbp)) ;
                   mawk_restore_ids(MAWK) ;
		   switch_code_to_main(MAWK) ;
                 }
#line 3094 "y.tab.c" /* yacc.c:1652  */
    break;

  case 169:
#line 1043 "parse.y" /* yacc.c:1652  */
    { mawk_eat_nl(MAWK, &yylval) ;
                   MAWK->scope = SCOPE_FUNCT ;
                   MAWK->active_funct = (yyvsp[-3].fbp) ;
                   *MAWK->main_code_p = MAWK->active_code ;

		   (yyvsp[-3].fbp)->nargs = (yyvsp[-1].ival) ;
                   if ( (yyvsp[-1].ival) )
                        (yyvsp[-3].fbp)->typev = (char *)
			memset( mawk_zmalloc(MAWK, (yyvsp[-1].ival)), ST_LOCAL_NONE, (yyvsp[-1].ival)) ;
                   else (yyvsp[-3].fbp)->typev = (char *) 0 ;

		   mawk_code_ptr = mawk_code_base =
                       (INST *) mawk_zmalloc(MAWK, INST_BYTES(PAGESZ));
		   mawk_code_limit = mawk_code_base + PAGESZ ;
		   mawk_code_warn = mawk_code_limit - CODEWARN ;
                 }
#line 3115 "y.tab.c" /* yacc.c:1652  */
    break;

  case 170:
#line 1062 "parse.y" /* yacc.c:1652  */
    { FBLOCK  *fbp ;

                   if ( (yyvsp[0].stp)->type == ST_NONE )
                   {
                         (yyvsp[0].stp)->type = ST_FUNCT ;
                         fbp = (yyvsp[0].stp)->stval.fbp = 
                             (FBLOCK *) mawk_zmalloc(MAWK, sizeof(FBLOCK)) ;
                         fbp->name = (yyvsp[0].stp)->name ;
			 fbp->code = (INST*) 0 ;
                   }
                   else
                   {
                         mawk_type_error(MAWK, (yyvsp[0].stp) ) ;

                         /* this FBLOCK will not be put in
                            the symbol table */
                         fbp = (FBLOCK*) mawk_zmalloc(MAWK, sizeof(FBLOCK)) ;
                         fbp->name = "" ;
                   }
                   (yyval.fbp) = fbp ;
                 }
#line 3141 "y.tab.c" /* yacc.c:1652  */
    break;

  case 171:
#line 1085 "parse.y" /* yacc.c:1652  */
    { (yyval.fbp) = (yyvsp[0].fbp) ; 
                   if ( (yyvsp[0].fbp)->code ) 
                       mawk_compile_error(MAWK, "redefinition of %s" , (yyvsp[0].fbp)->name) ;
                 }
#line 3150 "y.tab.c" /* yacc.c:1652  */
    break;

  case 172:
#line 1091 "parse.y" /* yacc.c:1652  */
    { (yyval.ival) = 0 ; }
#line 3156 "y.tab.c" /* yacc.c:1652  */
    break;

  case 174:
#line 1096 "parse.y" /* yacc.c:1652  */
    { (yyvsp[0].stp) = mawk_save_id(MAWK, (yyvsp[0].stp)->name) ;
                (yyvsp[0].stp)->type = ST_LOCAL_NONE ;
                (yyvsp[0].stp)->offset = 0 ;
                (yyval.ival) = 1 ;
              }
#line 3166 "y.tab.c" /* yacc.c:1652  */
    break;

  case 175:
#line 1102 "parse.y" /* yacc.c:1652  */
    { if ( is_local((yyvsp[0].stp)) ) 
                  mawk_compile_error(MAWK, "%s is duplicated in argument list",
                    (yyvsp[0].stp)->name) ;
                else
                { (yyvsp[0].stp) = mawk_save_id(MAWK, (yyvsp[0].stp)->name) ;
                  (yyvsp[0].stp)->type = ST_LOCAL_NONE ;
                  (yyvsp[0].stp)->offset = (yyvsp[-2].ival) ;
                  (yyval.ival) = (yyvsp[-2].ival) + 1 ;
                }
              }
#line 3181 "y.tab.c" /* yacc.c:1652  */
    break;

  case 176:
#line 1115 "parse.y" /* yacc.c:1652  */
    {  /* we may have to recover from a bungled function
		       definition */
		   /* can have local ids, before code scope
		      changes  */
		    mawk_restore_ids(MAWK) ;

		    switch_code_to_main(MAWK) ;
		 }
#line 3194 "y.tab.c" /* yacc.c:1652  */
    break;

  case 177:
#line 1127 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ;
             code2(MAWK, _CALL, (yyvsp[-2].fbp)) ;

             if ( (yyvsp[0].ca_p) )  code1((yyvsp[0].ca_p)->arg_num+1) ;
             else  code1(0) ;

             mawk_check_fcall(MAWK, (yyvsp[-2].fbp), MAWK->scope, MAWK->ps.code_move_level, MAWK->active_funct,  (yyvsp[0].ca_p), MAWK->token_lineno);
           }
#line 3207 "y.tab.c" /* yacc.c:1652  */
    break;

  case 178:
#line 1138 "parse.y" /* yacc.c:1652  */
    { (yyval.start) = (yyvsp[-1].start) ;
             code2(MAWK, _CALL, (yyvsp[-2].ptr)) ;

             if ( (yyvsp[0].ca_p) )  code1((yyvsp[0].ca_p)->arg_num+1) ;
             else  code1(0) ;

             mawk_check_ccall(MAWK, (yyvsp[-2].ptr), MAWK->scope, MAWK->ps.code_move_level, MAWK->active_funct,  (yyvsp[0].ca_p), MAWK->token_lineno);
           }
#line 3220 "y.tab.c" /* yacc.c:1652  */
    break;

  case 179:
#line 1149 "parse.y" /* yacc.c:1652  */
    { (yyval.ca_p) = (CA_REC *) 0 ; }
#line 3226 "y.tab.c" /* yacc.c:1652  */
    break;

  case 180:
#line 1151 "parse.y" /* yacc.c:1652  */
    { (yyval.ca_p) = (yyvsp[0].ca_p) ;
                 (yyval.ca_p)->link = (yyvsp[-1].ca_p) ;
                 (yyval.ca_p)->arg_num = (yyvsp[-1].ca_p) ? (yyvsp[-1].ca_p)->arg_num+1 : 0 ;
               }
#line 3235 "y.tab.c" /* yacc.c:1652  */
    break;

  case 181:
#line 1166 "parse.y" /* yacc.c:1652  */
    { (yyval.ca_p) = (CA_REC *) 0 ; }
#line 3241 "y.tab.c" /* yacc.c:1652  */
    break;

  case 182:
#line 1168 "parse.y" /* yacc.c:1652  */
    { (yyval.ca_p) = MAWK_ZMALLOC(MAWK, CA_REC) ;
                (yyval.ca_p)->link = (yyvsp[-2].ca_p) ;
                (yyval.ca_p)->type = CA_EXPR  ;
                (yyval.ca_p)->arg_num = (yyvsp[-2].ca_p) ? (yyvsp[-2].ca_p)->arg_num+1 : 0 ;
		(yyval.ca_p)->call_offset = mawk_code_offset ;
              }
#line 3252 "y.tab.c" /* yacc.c:1652  */
    break;

  case 183:
#line 1175 "parse.y" /* yacc.c:1652  */
    { (yyval.ca_p) = MAWK_ZMALLOC(MAWK, CA_REC) ;
                (yyval.ca_p)->link = (yyvsp[-2].ca_p) ;
                (yyval.ca_p)->arg_num = (yyvsp[-2].ca_p) ? (yyvsp[-2].ca_p)->arg_num+1 : 0 ;

                mawk_code_call_id(MAWK, (yyval.ca_p), (yyvsp[-1].stp)) ;
              }
#line 3263 "y.tab.c" /* yacc.c:1652  */
    break;

  case 184:
#line 1184 "parse.y" /* yacc.c:1652  */
    { (yyval.ca_p) = MAWK_ZMALLOC(MAWK, CA_REC) ;
                (yyval.ca_p)->type = CA_EXPR ;
		(yyval.ca_p)->call_offset = mawk_code_offset ;
              }
#line 3272 "y.tab.c" /* yacc.c:1652  */
    break;

  case 185:
#line 1190 "parse.y" /* yacc.c:1652  */
    { (yyval.ca_p) = MAWK_ZMALLOC(MAWK, CA_REC) ;
                mawk_code_call_id(MAWK, (yyval.ca_p), (yyvsp[-1].stp)) ;
              }
#line 3280 "y.tab.c" /* yacc.c:1652  */
    break;

  case 186:
#line 1197 "parse.y" /* yacc.c:1652  */
    { mawk_parser_include(MAWK, (yyvsp[0].ptr)); }
#line 3286 "y.tab.c" /* yacc.c:1652  */
    break;


#line 3290 "y.tab.c" /* yacc.c:1652  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (MAWK, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (MAWK, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, MAWK);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, MAWK);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (MAWK, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, MAWK);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, MAWK);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1205 "parse.y" /* yacc.c:1918  */


/* resize the code for a user function */

static void  resize_fblock(mawk_state_t *MAWK, FBLOCK *fbp)
{
  CODEBLOCK *p = MAWK_ZMALLOC(MAWK, CODEBLOCK) ;

  mawk_code2op(MAWK, _RET0, _HALT) ;
	/* make sure there is always a return */

  *p = MAWK->active_code ;
  fbp->code = mawk_code_shrink(MAWK, p, &fbp->size) ;
      /* mawk_code_shrink() zfrees p */

/* this list is alos used to free functions in pedantic mode */
#ifndef MAWK_MEM_PEDANTIC
	if ( MAWK->dump_code_flag )
#endif
		mawk_add_to_fdump_list(MAWK, fbp) ;
/*	printf("CODE add: %p/%d\n", fbp->code, fbp->size);*/
}


/* convert FE_PUSHA  to  FE_PUSHI
   or F_PUSH to F_PUSHI
*/
static void  field_A2I(mawk_state_t *MAWK)
{
	mawk_cell_t *cp;

  if ( mawk_code_ptr[-1].op == FE_PUSHA &&
       mawk_code_ptr[-1].ptr == (PTR) 0)
  /* On most architectures, the two mawk_tests are the same; a good
     compiler might eliminate one.  On LM_DOS, and possibly other
     segmented architectures, they are not */
  { mawk_code_ptr[-1].op = FE_PUSHI ; }
  else
  {
    cp = (mawk_cell_t *) mawk_code_ptr[-1].ptr ;

    if (cp == MAWK->field  || (cp > MAWK_NF && cp <= LAST_PFIELD))
    {
         mawk_code_ptr[-2].op = _PUSHI  ;
    }
    else if ( cp == MAWK_NF )
    { mawk_code_ptr[-2].op = NF_PUSHI ; mawk_code_ptr-- ; }

    else
    { 
      mawk_code_ptr[-2].op = F_PUSHI ;
      mawk_code_ptr -> op = mawk_field_addr_to_index(MAWK, mawk_code_ptr[-1].ptr ) ;
      mawk_code_ptr++ ;
    }
  }
}

/* we've seen an ID in a context where it should be a VAR,
   check that's consistent with previous usage */
static void check_var(mawk_state_t *MAWK, register SYMTAB *p)
{
      switch(p->type)
      {
        case ST_NONE : /* new id */
            p->type = ST_VAR ;
            p->stval.cp = MAWK_ZMALLOC(MAWK, mawk_cell_t) ;
            p->stval.cp->type = C_NOINIT ;
            break ;

        case ST_LOCAL_NONE :
            p->type = ST_LOCAL_VAR ;
            MAWK->active_funct->typev[p->offset] = ST_LOCAL_VAR ;
            break ;

        case ST_VAR :
        case ST_LOCAL_VAR :  break ;

        default :
            mawk_type_error(MAWK, p) ;
            break ;
      }
}

/* we've seen an ID in a context where it should be an ARRAY,
   check that's consistent with previous usage */
static  void  check_array(mawk_state_t *MAWK, register SYMTAB *p)
{
      switch(p->type)
      {
        case ST_NONE :  /* a new array */
            p->type = ST_ARRAY ;
            p->stval.array = mawk_array_new(MAWK, NULL) ;
            break ;

        case  ST_ARRAY :
        case  ST_LOCAL_ARRAY :
            break ;

        case  ST_LOCAL_NONE  :
            p->type = ST_LOCAL_ARRAY ;
            MAWK->active_funct->typev[p->offset] = ST_LOCAL_ARRAY ;
            break ;

        default : mawk_type_error(MAWK, p) ; break ;
      }
}

static void mawk_code_array(mawk_state_t *MAWK, register SYMTAB *p)
{
  if ( is_local(p) ) mawk_code2op(MAWK, LA_PUSHA, p->offset) ; 
  else  code2(MAWK, A_PUSHA, p->stval.array) ;
}


/* we've seen an ID as an argument to a user defined function */
static void mawk_code_call_id(mawk_state_t *MAWK, register CA_REC *p, register SYMTAB *ip)
{
  p->call_offset = mawk_code_offset ;
     /* This always get set now.  So that fcall:relocate_arglist
	works. */

  switch( ip->type )
  {
    case  ST_VAR  :
            p->type = CA_EXPR ;
            code2(MAWK, _PUSHI, ip->stval.cp) ;
            break ;

    case  ST_LOCAL_VAR  :
            p->type = CA_EXPR ;
            mawk_code2op(MAWK, L_PUSHI, ip->offset) ;
            break ;

    case  ST_ARRAY  :
            p->type = CA_ARRAY ;
            code2(MAWK, A_PUSHA, ip->stval.array) ;
            break ;

    case  ST_LOCAL_ARRAY :
            p->type = CA_ARRAY ;
            mawk_code2op(MAWK, LA_PUSHA, ip->offset) ;
            break ;

    /* not enough info to code it now; it will have to
       be patched later */

    case  ST_NONE :
            p->type = ST_NONE ;
            p->sym_p = ip ;
            code2(MAWK, _PUSHI, &MAWK->code_call_id_dummy) ;
            break ;

    case  ST_LOCAL_NONE :
            p->type = ST_LOCAL_NONE ;
            p->type_p = & MAWK->active_funct->typev[ip->offset] ;
            mawk_code2op(MAWK, L_PUSHI, ip->offset) ;
            break ;

#ifdef   DEBUG
    default :
            mawk_bozo(MAWK, "mawk_code_call_id") ;
#endif

  }
}

/* an RE by itself was coded as _MATCH0 , change to
   push as an expression */

static void RE_as_arg(mawk_state_t *MAWK)
{
	mawk_cell_t *cp = MAWK_ZMALLOC(MAWK, mawk_cell_t) ;

  mawk_code_ptr -= 2 ;
  cp->type = C_RE ;
  cp->ptr = mawk_code_ptr[1].ptr ;
  code2(MAWK, _PUSHC, cp) ;
}

/* reset the active_code back to the MAIN block */
static void switch_code_to_main(mawk_state_t *MAWK)
{
   switch(MAWK->scope)
   {
     case SCOPE_BEGIN :
	*MAWK->begin_code_p = MAWK->active_code ;
	MAWK->active_code = *MAWK->main_code_p ;
	break ;

     case SCOPE_END :
	*MAWK->end_code_p = MAWK->active_code ;
	MAWK->active_code = *MAWK->main_code_p ;
	break ;

     case SCOPE_FUNCT :
	MAWK->active_code = *MAWK->main_code_p ;
	break ;

     case SCOPE_MAIN :
	break ;
   }
   MAWK->active_funct = (FBLOCK*) 0 ;
   MAWK->scope = SCOPE_MAIN ;
}


void mawk_parse(mawk_state_t *MAWK)
{
	if (!MAWK->binary_loaded) {
   if ( yyparse(MAWK) || MAWK->compile_error_count != 0 ) mawk_exit(MAWK, 2) ;

   mawk_scan_cleanup(MAWK) ;
   mawk_set_code(MAWK) ;
   /* code must be set before call to mawk_resolve_fcalls() */
   if ( MAWK->resolve_list )  mawk_resolve_fcalls(MAWK) ;
	}

   if ( MAWK->compile_error_count != 0 ) mawk_exit(MAWK, 2) ;
   if ( MAWK->dump_code_flag ) { mawk_dump_code(MAWK);}
   if ( MAWK->dump_sym_flag ) { mawk_dump_sym_text(MAWK); }
   if ((MAWK->dump_code_flag ) || ( MAWK->dump_sym_flag )) { mawk_exit(MAWK, 0); }

	(void)mawk_d2f(NULL); /* suppress compiler warning */
}


void mawk_parser_include(mawk_state_t *MAWK, void *str)
{
	mawk_parser_push(MAWK);

	MAWK->ps.eof_flag = 0 ;
	MAWK->ps.pfile_name = ((mawk_string_t *)str)->str;
	MAWK->ps.buffp = MAWK->ps.buffer = (unsigned char *) mawk_zmalloc(MAWK, BUFFSZ + 1) ;
	*MAWK->ps.buffp = '\0';
	if (mawk_scan_open(MAWK) == 1)
		MAWK->token_lineno = MAWK->lineno = 1 ;
	else
		mawk_parser_pop(MAWK);
}
