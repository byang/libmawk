/* Never Standard.h

   This has all the prototypes that are supposed to
   be in a standard place but never are, and when they are
   the standard place isn't standard
*/

#ifndef  NSTD_H
#define  NSTD_H		1


/* types */
typedef void *PTR;

#include "conf.h"

/* stdlib.h */
#ifndef MAWK_NO_FLOAT
double strtod(const char *, char **);
#endif

#ifdef MAWK_BROKEN_STDLIB
void free(void *);
PTR malloc(size_t);
PTR realloc(void *, size_t);
void exit(int);
#endif

char *getenv(const char *);

/* string.h */

int memcmp(const void *, const void *, size_t);
PTR memcpy(void *, const void *, size_t);
PTR memset(void *, int, size_t);
char *strcpy(char *, const char *);
size_t strlen(const char *);
char *strerror(int);


#ifdef  NO_ERRNO_H
extern int errno;
#else
#include <errno.h>
#endif

/* math.h */
/* if have to diddle with errno to get errors from the math library */
#ifndef STDC_MATHERR
#define STDC_MATHERR   (FPE_TRAPS_ON && NO_MATHERR)
#endif

#endif /* NSTD_H */
