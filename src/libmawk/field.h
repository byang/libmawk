
/********************************************
field.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  FIELD_H
#define  FIELD_H   1

void mawk_set_field0(mawk_state_t *, char *, unsigned);
void mawk_split_field0(mawk_state_t *);
int mawk_space_split(mawk_state_t *, char *, unsigned);
int mawk_re_split(mawk_state_t *, char *, PTR);
int mawk_null_split(mawk_state_t *, char *);
void mawk_field_assign(mawk_state_t *, mawk_cell_t *, mawk_cell_t *);
char *mawk_is_string_split(PTR, unsigned *);
void mawk_bifunct_target_assign(mawk_state_t *, mawk_cell_t *, mawk_cell_t *);
mawk_cell_t *mawk_slow_field_ptr(mawk_state_t *, int);
int mawk_field_addr_to_index(mawk_state_t *, mawk_cell_t *);
void set_binmode(int);
void mawk_load_pfield(mawk_state_t *, char *, mawk_cell_t *);

#ifdef MAWK_MEM_PEDANTIC
void mawk_field_uninit(mawk_state_t * MAWK);
#endif

/* some compilers choke on (NF-field) in a case statement
   even though it's constant so ...
*/
#define  MAWK_NF_field       (MAX_SPLIT+1)
#define  MAWK_RS_field       (MAX_SPLIT+2)
#define  MAWK_FS_field       (MAX_SPLIT+3)
#define  MAWK_CONVFMT_field  (MAX_SPLIT+4)
#define  MAWK_OFMT_field     (MAX_SPLIT+5)

/* index to mawk_cell_t *  for a field */
#define field_ptr(i) ((i)<=MAX_SPLIT ? MAWK->field+(i):mawk_slow_field_ptr(MAWK, i))

/* the pseudo fields, assignment has side effects */
#define  MAWK_NF       (MAWK->field+MAX_SPLIT+1)  /* must be first */
#define  MAWK_RS       (MAWK->field+MAX_SPLIT+2)
#define  MAWK_FS       (MAWK->field+MAX_SPLIT+3)
#define  MAWK_CONVFMT  (MAWK->field+MAX_SPLIT+4)
#define  MAWK_OFMT     (MAWK->field+MAX_SPLIT+5)  /* must be last */

#define  LAST_PFIELD   MAWK_OFMT

/* a shadow type for RS and FS */
#define  SEP_SPACE      0
#define  SEP_CHAR       1
#define  SEP_STR        2
#define  SEP_RE         3
#define  SEP_MLR	4

/*  types for splitting mawk_overflow */


#endif /* FIELD_H  */
