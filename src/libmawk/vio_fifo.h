#ifndef MAWK_VIO_FIFO_H
#define MAWK_VIO_FIFO_H

#include "mawk.h"
#include "vio.h"
#include "zfifo.h"

typedef struct mawk_vio_fifo_s {
	mawk_vio_t vio_common_head;
	mawk_zfifo_t fifo;
	int is_awk2app;         /* 1 if pipe is awk->app */
	int is_stdout;          /* 1 if pipe is an stdout */
	int eof_from_awk;       /* 1 if there won't be more from awk or awk won't accept more data (close()) */
	int eof_from_app;       /* 1 if there won't be more from the app or the app won't accept more data */
} mawk_vio_fifo_t;

const mawk_vio_imp_t mawk_vio_fifo_imp;

mawk_vio_t *mawk_vio_fifo_open(mawk_state_t *MAWK, const char *name, mawk_vio_open_mode_t mode);


int mawk_vio_fifo_eof_from_app(mawk_state_t *MAWK, mawk_vio_t *vf);

/* app writes an input buffer */
int mawk_vio_fifo_write_app(mawk_state_t *MAWK, mawk_vio_t *vf, const char *data, int len);

/* app reads an output buffer */
int mawk_vio_fifo_read_app(mawk_state_t *MAWK, mawk_vio_t *vf, char *dst, long int size);

#endif
