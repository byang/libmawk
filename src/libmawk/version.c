
/********************************************
version.c

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991-95.  Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include "mawk.h"
#include "vio_orig.h"

/* mawk 1.3 */
#define  PATCHLEVEL	3
#define  PATCH_STRING	".3"
#define  DATE_STRING    "Nov 1996"
#define  MAWK_ID	"@(#)mawk 1.3.3"

#define	 VERSION_STRING	 \
	"lmawk " LMAWK_VER ", Copyright (C) Tibor 'Igor2' Palinkas\n" \
	" (http://repo.hu/projects/libmawk; email: libmawk (at) igor2.repo.hu)\n" \
  " based on mawk 1.3%s %s, Copyright (C) Michael D. Brennan\n\n"


static const char fmt[] = "%-14s%10lu\n";

/* print VERSION and exit */
void mawk_print_version(mawk_state_t * MAWK)
{
	mawk_vio_orig_setup_stdio(MAWK, 0, 1, 1);

	MAWK->fnode_stdout->vf->imp->vprintf(MAWK, MAWK->fnode_stdout->vf, VERSION_STRING, PATCH_STRING, DATE_STRING);
	mawk_vio_flush(MAWK, MAWK->fnode_stdout->vf);

	MAWK->fnode_stdout->vf->imp->vprintf(MAWK, MAWK->fnode_stderr->vf, "compiled limits:\n");
	MAWK->fnode_stdout->vf->imp->vprintf(MAWK, MAWK->fnode_stderr->vf, fmt, "max NF", (long) MAX_FIELD);
	MAWK->fnode_stdout->vf->imp->vprintf(MAWK, MAWK->fnode_stderr->vf, fmt, "sprintf buffer", (long) SPRINTF_SZ);
	exit(0);
}

