
/********************************************
symtype.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

/* types related to symbols are defined here */

#ifndef  SYMTYPE_H
#define  SYMTYPE_H

/*---------------------------
   structures and types for arrays
 *--------------------------*/

#include <libmawk/array.h>

/* for parsing  (i,j) in A  */
typedef struct {
	int start;										/* offset to code_base */
	int cnt;
} ARG2_REC;

void mawk_add_to_fdump_list(mawk_state_t *, FBLOCK *);
int mawk_fdump(mawk_state_t * MAWK);

/*-------------------------
  elements of the symbol table
  -----------------------*/

#define  ST_NONE 0
#define  ST_VAR   1
#define  ST_KEYWORD   2
#define  ST_BUILTIN 3						/* a pointer to a builtin record */
#define  ST_ARRAY   4						/* a void * ptr to a mawk_hash table */
#define  ST_FIELD   5						/* a cell ptr to a field */
#define  ST_FUNCT   6
#define  ST_NR      7						/*  NR is special */
/*       ST_ENV     8						used to be ENVIRON[] before virtual array support*/
#define  ST_LENGTH  9						/* ditto and mawk_bozo */
#define  ST_LOCAL_NONE  10
#define  ST_LOCAL_VAR   11
#define  ST_LOCAL_ARRAY 12
#define  ST_C_FUNCTION  13			/* call from the awk script to a c function - stores a function pointer */

#define  is_local(stp)   ((stp)->type>=ST_LOCAL_NONE)


/*****************************
 structures for type checking function calls
 ******************************/

typedef struct ca_rec {
	struct ca_rec *link;
	short type;
	short arg_num;								/* position in callee's stack */
/*---------  this mawk_data only set if we'll  need to patch -------*/
/* happens if argument is an ID or type ST_NONE or ST_LOCAL_NONE */

	int call_offset;
/* where the type is stored */
	SYMTAB *sym_p;								/* if type is ST_NONE  */
	char *type_p;									/* if type  is ST_LOCAL_NONE */
} CA_REC;												/* call argument record */

/* type field of CA_REC matches with ST_ types */
#define   CA_EXPR       ST_LOCAL_VAR
#define   CA_ARRAY      ST_LOCAL_ARRAY

struct fcall {
	struct fcall *link;
	FBLOCK *callee;
	short call_scope;
	short move_level;
	FBLOCK *call;									/* only used if call_scope == SCOPE_FUNCT  */
	INST *call_start;							/* computed later as code may be moved */
	CA_REC *arg_list;
	short arg_cnt_checked;
	unsigned line_no;							/* for error messages */
};

extern FCALL_REC *resolve_list;

void mawk_resolve_fcalls(mawk_state_t * MAWK);
void mawk_check_fcall(mawk_state_t *, FBLOCK *, int, int, FBLOCK *, CA_REC *, unsigned);
void mawk_check_ccall(mawk_state_t *, FBLOCK *, int, int, FBLOCK *, CA_REC *, unsigned);
void mawk_relocate_resolve_list(mawk_state_t *MAWK, int, int, FBLOCK *, int, unsigned, int);

/* mawk_hash.c */
unsigned mawk_hash(const char *);

/* register a symbol; name is not copied, the pointer is stored in the table! */
SYMTAB *mawk_insert(mawk_state_t *, const char *name);

#ifdef MAWK_MEM_PEDANTIC
/* remove a symbol */
void mawk_delete(mawk_state_t *MAWK, const char *name, int cell_destroy);
#endif

SYMTAB *mawk_find(mawk_state_t *, const char *, int);
extern const char *mawk_reverse_uk;
const char *mawk_reverse_find(mawk_state_t *, int, PTR);
SYMTAB *mawk_save_id(mawk_state_t * MAWK, const char *);
void mawk_restore_ids(mawk_state_t * MAWK);

/* error.c */
void mawk_type_error(mawk_state_t *, SYMTAB *);

#endif /* SYMTYPE_H */
