/********************************************
libmawk (C) 2014, Tibor 'Igor2' Palinkas;

This is a source file for libmawk, an implementation of
the AWK programming language, fork of mawk.

Libmawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include "mawk.h"
#include "field.h"

extern int isatty(int);

void mawk_detect_interactive(mawk_state_t *MAWK)
{
	if (isatty(0) && MAWK->rs_shadow.type == SEP_CHAR && MAWK->rs_shadow.c == '\n') {
		/* interactive line buffered mode */
		MAWK->interactive_flag = 1;
	}
}
