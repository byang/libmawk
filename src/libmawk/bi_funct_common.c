
/********************************************
bi_funct_common.c

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/
#include "mawk.h"
#include "bi_funct.h"
#include "bi_vars.h"


/* global for the disassembler */
BI_REC const mawk_bi_funct[] = {	/* info to load builtins */
	{"length", mawk_bi_length, 0, 1},	/* special must come first */
	{"index", mawk_bi_index, 2, 2},
	{"substr", mawk_bi_substr, 2, 3},
	{"sprintf", mawk_bi_sprintf, 1, 255},
#ifndef MAWK_NO_FLOAT
	{"sin", mawk_bi_sin, 1, 1},
	{"cos", mawk_bi_cos, 1, 1},
	{"atan2", mawk_bi_atan2, 2, 2},
	{"exp", mawk_bi_exp, 1, 1},
	{"log", mawk_bi_log, 1, 1},
#endif
	{"int", mawk_bi_int, 1, 1},
	{"sqrt", mawk_bi_sqrt, 1, 1},
	{"rand", mawk_bi_rand, 0, 0},
	{"srand", mawk_bi_srand, 0, 1},
	{"close", mawk_bi_close, 1, 1},
#ifndef MAWK_NO_FORK
	{"system", mawk_bi_system, 1, 1},
#endif
	{"toupper", mawk_bi_toupper, 1, 1},
	{"tolower", mawk_bi_tolower, 1, 1},
	{"fflush", mawk_bi_fflush, 0, 1},
	{"call", mawk_bi_call, 1, 255},
	{"acall", mawk_bi_acall, 2, 2},
	{"valueof", mawk_bi_valueof, 1, 2},
	{"isnan", mawk_bi_isnan, 1, 1},
	{(char *) 0, (PF_CP) 0, 0, 0}
};


/* load built-in functions in symbol table */
void mawk_bi_funct_init(mawk_state_t *MAWK)
{
	const register BI_REC *p;
	register SYMTAB *stp;

	/* length is special (posix mawk_bozo) */
	stp = mawk_insert(MAWK, mawk_bi_funct->name);
	stp->type = ST_LENGTH;
	stp->stval.bip = mawk_bi_funct;

	for (p = mawk_bi_funct + 1; p->name; p++) {
		stp = mawk_insert(MAWK, p->name);
		stp->type = ST_BUILTIN;
		stp->stval.bip = p;
	}

	/* seed rand() off the clock */
	{
		mawk_cell_t c;

		c.type = 0;
		mawk_bi_srand(MAWK, &c);
	}

}

#ifdef MAWK_MEM_PEDANTIC
void mawk_bi_funct_uninit(mawk_state_t *MAWK)
{
	const register BI_REC *p;
	mawk_delete(MAWK, mawk_bi_funct->name, 0);

	for (p = mawk_bi_funct + 1; p->name; p++)
		mawk_delete(MAWK, p->name, 0);
}
#endif
