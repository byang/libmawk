
/********************************************
kw.c

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include "mawk.h"
#include "symtype.h"
#include "parse.h"
#include "init.h"

static const struct kw {
	char *text;
	short kw;
} keywords[] = { /* read-only */
	{"print",    PRINT},
	{"printf",   PRINTF},
	{"do",       DO},
	{"while",    WHILE},
	{"for",      FOR},
	{"break",    BREAK},
	{"continue", CONTINUE},
	{"if",       IF},
	{"else",     ELSE},
	{"in",       IN},
	{"delete",   DELETE},
	{"split",    SPLIT},
	{"match",    MATCH_FUNC},
	{"BEGIN",    BEGIN},
	{"END",      END},
	{"include",  INCLUDE},
	{"exit",     EXIT},
	{"next",     NEXT},
	{"return",   RETURN},
	{"getline",  GETLINE},
	{"sub",      SUB},
	{"gsub",	   GSUB},
	{"function", FUNCTION},
	{NULL,       0}
};

/* put keywords in the symbol table */
void mawk_kw_init(mawk_state_t * MAWK)
{
	register const struct kw *p = keywords;
	register SYMTAB *q;

	while (p->text) {
		q = mawk_insert(MAWK, p->text);
		q->type = ST_KEYWORD;
		q->stval.kw = p++->kw;
	}
}

#ifdef MAWK_MEM_PEDANTIC
void mawk_kw_uninit(mawk_state_t * MAWK)
{
	register const struct kw *p = keywords;
	register SYMTAB *q;

	while (p->text) {
		mawk_delete(MAWK, p->text, 0);
		p++;
	}
}
#endif

/* mawk_find a keyword to emit an error message */
const char *mawk_find_kw_str(int kw_token)
{
	const struct kw *p;

	for (p = keywords; p->text; p++)
		if (p->kw == kw_token)
			return p->text;
	/* search failed */
	return (char *) 0;
}
