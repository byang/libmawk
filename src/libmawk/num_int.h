/* number type */
typedef int mawk_num_t;

/* constant format differ for floating point and int */
#define MAWK_NUM_ZERO 0
#define MAWK_NUM_ONE  1
#define NUM_NAN 0x80000000

/* default printf format */
#define NUM_FMT "%d"

/* format for the disassembler */
#define NUM_FMT_DA "%d"

mawk_num_t mawk_num_pow(mawk_num_t x, mawk_num_t y);
mawk_num_t mawk_num_sqrt(mawk_num_t x);
#define mawk_num_int(d) (d)

#define P_isnan(x)  ((x) == NUM_NAN)
#define P_nan(x)    (NUM_NAN)

#define P_isnan_manual(x) P_isnan(x)

#define strtonum(nptr, endptr) strtol(nptr, endptr, 10)

