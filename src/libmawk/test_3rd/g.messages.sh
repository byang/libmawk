fn=messages

echo "*** stdout: " >$fn.out
echo "" | $AWK -f $GAWK_DIR/$fn.awk $fn.in >>$fn.out 2>$fn.fd2
(echo "*** stderr: "; cat $fn.fd2) >>$fn.out
(echo "*** _out1: "; cat _out1) >>$fn.out

dif=`diff -u "local.$implementation/$fn.ok" "$fn.out"`
announce "$?" "$fn" "$dif" "$fn.out"
rm $fn.fd2 _out1
