fn=iobug1

echo "" | $AWK -f $GAWK_DIR/$fn.awk >$fn.out 2>&1
dif=`diff -u "$GAWK_DIR/$fn.ok" "$fn.out"`
announce "$?" "$fn" "$dif" "$fn.out"

