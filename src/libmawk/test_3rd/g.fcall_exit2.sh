fn=fcall_exit2

echo "" >$fn.out
echo "oops" > 2
for i in 1 2
do
	echo "*** $i" >> $fn.out
	echo "" | $AWK -f $GAWK_DIR/$fn.awk $GAWK_DIR/$fn.in $i >>$fn.out 2>&1
	echo "EXITVAL=$?" >> $fn.out
done
dif=`diff -u "local.$implementation/$fn.ok" "$fn.out"`
announce "$?" "$fn" "$dif" "$fn.out"
rm 2

