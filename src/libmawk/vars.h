/********************************************
vars.h

libmawk changes (C) 2009-2012, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include "mawk.h"

/* look up an existing variable */
mawk_cell_t *mawk_get_var(mawk_state_t *MAWK, const char *vname);

/* create a variable and return mawk_cell_t; overwrite/clear existing variable;
   return NULL if an object that can not be overwritten already exist on this
   name. fp should be NULL if field assignment is not needed */
mawk_cell_t *mawk_create_var(mawk_state_t *MAWK, const char *name, mawk_cell_t **fp);
