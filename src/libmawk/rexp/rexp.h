
/********************************************
rexp.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  REXP_H
#define  REXP_H

#include "libmawk/nstd.h"
#include "libmawk/mawk.h"
#include <stdio.h>

PTR mawk_RE_malloc(mawk_state_t *MAWK, unsigned);
PTR mawk_RE_realloc(mawk_state_t *MAWK, void *, unsigned, unsigned);


/*  finite machine  state types  */

#define  M_STR     	0
#define  M_CLASS   	1
#define  M_ANY     	2
#define  M_START   	3
#define  M_END     	4
#define  M_U       	5
#define  M_1J      	6
#define  M_2JA     	7
#define  M_2JB     	8
#define  M_ACCEPT  	9
#define  U_ON      	10

#define  U_OFF     0
#define  END_OFF   0
#define  END_ON    (2*U_ON)


#define  STATESZ  (sizeof(mawk_RESTATE))

typedef struct {
	mawk_RESTATE *start, *stop;
	int size;                 /* how many bytes are allocated for start */
} MACHINE;
#define is_invm(m) (((m).start == NULL) && ((m).stop == NULL))


/*  tokens   */
#define  T_OR   1								/* | */
#define  T_CAT  2
#define  T_STAR 3								/* * */
#define  T_PLUS 4								/* + */
#define  T_Q    5								/* ? */
#define  T_LP   6								/* ( */
#define  T_RP   7								/* ) */
#define  T_START 8							/* ^ */
#define  T_END  9								/* $ */
#define  T_ANY  10							/* . */
#define  T_CLASS 11							/* starts with [ */
#define  T_SLASH 12							/*  \  */
#define  T_CHAR  13							/* all the rest */
#define  T_STR   14
#define  T_U     15

/*  precedences and error codes  */
#define  L   0
#define  EQ  1
#define  G   2
#define  E1  (-1)
#define  E2  (-2)
#define  E3  (-3)
#define  E4  (-4)
#define  E5  (-5)
#define  E6  (-6)
#define  E7  (-7)

#define  MEMORY_FAILURE      5

#define  ison(b,x)  ((b)[((unsigned char)(x))>>3] & (1<<((x)&7)))

/*  error  trap   */
MACHINE mawk_RE_u(mawk_state_t *MAWK);
MACHINE mawk_RE_start(mawk_state_t *MAWK);
MACHINE mawk_RE_end(mawk_state_t *MAWK);
MACHINE mawk_RE_any(mawk_state_t *MAWK);
MACHINE mawk_RE_str(mawk_state_t *MAWK, char *, unsigned);
MACHINE mawk_RE_class(mawk_state_t *MAWK, mawk_BV *);
int mawk_RE_cat(mawk_state_t *MAWK, MACHINE *, MACHINE *);
int mawk_RE_or(mawk_state_t *MAWK, MACHINE *, MACHINE *);
int mawk_RE_close(mawk_state_t *MAWK, MACHINE *);
int mawk_RE_poscl(mawk_state_t *MAWK, MACHINE *);
int mawk_RE_01(mawk_state_t *MAWK, MACHINE *);
void mawk_RE_panic(char *);
char *mawk_str_str(char *, char *, unsigned);

int mawk_RE_lex_init(mawk_state_t *MAWK, char *);
int mawk_RE_lex(mawk_state_t *MAWK, MACHINE *);
int mawk_RE_run_stack_init(mawk_state_t *MAWK);
mawk_RT_STATE *mawk_RE_new_run_stack(mawk_state_t *MAWK);

void mawk_RE_free(mawk_state_t *MAWK, PTR p, unsigned sz);


#endif /* REXP_H  */
