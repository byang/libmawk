void mawk_array_clear_generic(mawk_state_t *, mawk_array_t);
mawk_string_t **mawk_array_loop_vector_generic(mawk_state_t *, mawk_array_t, unsigned *vsize);
void mawk_array_load_generic(mawk_state_t *, mawk_array_t, int cnt);

