#ifndef DA_BIN_H
#define DA_BIN_H
#include  "mawk.h"

/* load a precompiled script, using the provided read() on fd. Read will take
   place sequentially, typically in small chunks */
int mawk_load_code_bin_(mawk_state_t *MAWK, void *fd, int (*read)(void *fd, void *buff, size_t len));

/* dump a precompiled script in binary form using the provided write() on fd.
   Write will take place sequentially, in small chunks. */
int mawk_save_code_bin_(mawk_state_t * MAWK, void *fd, int (*write)(void *fd, const void *buff, size_t len));

#endif
