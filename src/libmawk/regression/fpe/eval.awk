# check output for nans
/[nN][aA][nN]|[?]/ { nan++ }

END {
	if (r1 > 128) print "test1 failed"
	if (r2 > 128) print "test2 failed"
	if (r3 > 128) print "test3 failed"

# return values should all be 0 if ignoring FPEs (e.g. with IEEE754)
# or all 2 if trapping FPEs
	if ((r1 == r2) && (r2 == r3)) {
		print "consistent FPE results: all ", r1
		if (r1 == 0)
			print "style: ignoring floating exceptions"
		else
			print "style: trapping floating exceptions"
	}
	else
		print "inconsistent FPE results: ", r1, r2, r3

# test3 must print nan
	if (!nan) {
		print "but the library is not IEEE754 compatible"
		print "test 3 failed"
	}
}
