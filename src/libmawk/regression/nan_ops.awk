BEGIN { nan=log(-1) }

# op=, variables
{
	a = 1
	a += nan
	print a

	a = 1
	a -= nan
	print a

	a = 1
	a *= nan
	print a

	a = 1
	a /= nan
	print a
}

# op=, fields have their on path in the code
{
	$1 = 1
	$1 += nan
	print $1

	$1 = 1
	$1 -= nan
	print $1

	$1 = 1
	$1 *= nan
	print $1

	$1 = 1
	$1 /= nan
	print $1
}

# inc/dec for vars and fields
{
	a = nan
	a++
	print a

	a = nan
	a--
	print a

	a = nan
	++a
	print a

	a = nan
	--a
	print a

	$1 = nan
	$1++
	print $1

	$1 = nan
	$1--
	print $1

	$1 = nan
	++$1
	print $1

	$1 = nan
	--$1
	print $1

}

{
	# enough to test once
	exit 0
}
