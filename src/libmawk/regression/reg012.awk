# used to be test/reg[012].awk

/return/ {cnt[0]++}

/return|switch/ {cnt[1]++}

/[A-Za-z_][A-Za-z0-9_]*\[.*\][ \t]*=/ {cnt[2]++}

END{print "return=" cnt[0], "return|switch=" cnt[1], "array=" cnt[2]}
