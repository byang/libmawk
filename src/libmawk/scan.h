
/********************************************
scan.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef  SCAN_H_INCLUDED
#define  SCAN_H_INCLUDED   1


#ifndef   MAKESCAN
#include  <libmawk/symtype.h>
#include  <libmawk/parse.h>

void mawk_parser_push(mawk_state_t * MAWK);
int mawk_parser_pop(mawk_state_t * MAWK);
int mawk_scan_open(mawk_state_t * MAWK);

void mawk_parse(mawk_state_t *);
int Mawk_lex(YYSTYPE *lvalp, mawk_state_t *);
int Mawk_parse(mawk_state_t *);
void Mawk_error(mawk_state_t *MAWK, char *s_unused);
void mawk_scan_cleanup(mawk_state_t *);
void mawk_unexpected_char(mawk_state_t * MAWK, YYSTYPE *lvalp);
#endif


extern const char mawk_scan_code[256];	/* read-only */

/*  the scan codes to compactify the main switch */

#define  SC_SPACE               1
#define  SC_NL                  2
#define  SC_SEMI_COLON          3
#define  SC_FAKE_SEMI_COLON     4
#define  SC_LBRACE              5
#define  SC_RBRACE              6
#define  SC_QMARK               7
#define  SC_COLON               8
#define  SC_OR                  9
#define  SC_AND                10
#define  SC_PLUS               11
#define  SC_MINUS              12
#define  SC_MUL                13
#define  SC_DIV                14
#define  SC_MOD                15
#define  SC_POW                16
#define  SC_LPAREN             17
#define  SC_RPAREN             18
#define  SC_LBOX               19
#define  SC_RBOX               20
#define  SC_IDCHAR             21
#define  SC_DIGIT              22
#define  SC_DQUOTE             23
#define  SC_ESCAPE             24
#define  SC_COMMENT            25
#define  SC_EQUAL              26
#define  SC_NOT                27
#define  SC_LT                 28
#define  SC_GT                 29
#define  SC_COMMA              30
#define  SC_DOT                31
#define  SC_MATCH              32
#define  SC_DOLLAR             33
#define  SC_UNEXPECTED         34
#define  SC_INCLUDE            35

#ifndef  MAKESCAN

void mawk_eat_nl(mawk_state_t * MAWK, YYSTYPE *lvalp);

#define  ct_ret(x)  return MAWK->current_token = (x)

#define  next(MAWK) (*MAWK->ps.buffp ? *MAWK->ps.buffp++ : slow_next(MAWK))
#define  un_next()  MAWK->ps.buffp--

#define  mawk_test1_ret(c,x,d)  if ( next(MAWK) == (c) ) ct_ret(x) ;\
                           else { un_next() ; ct_ret(d) ; }

#define  mawk_test2_ret(c1,x1,c2,x2,d)   switch( next(MAWK) )\
                                   { case c1: ct_ret(x1) ;\
                                     case c2: ct_ret(x2) ;\
                                     default: un_next() ;\
                                              ct_ret(d) ; }
#endif /* ! MAKESCAN  */


#endif
