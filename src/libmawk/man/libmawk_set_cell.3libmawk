.\" Copyright 2009 Tibor Palinkas (mawk@inno.bme.hu)
.\"
.\" Permission is granted to make and distribute verbatim copies of this
.\" manual provided the copyright notice and this permission notice are
.\" preserved on all copies.
.\"
.\" Permission is granted to copy and distribute modified versions of this
.\" manual under the conditions for verbatim copying, provided that the
.\" entire resulting derived work is distributed under the terms of a
.\" permission notice identical to this one.
.\"
.\" Formatted or processed versions of this manual, if unaccompanied by
.\" the source, must acknowledge the copyright and authors of this work.
.\"
.TH LIBMAWK_SET_CELL 3libmawk  2009-08-10 "libmawk" "libmawk manual"
.SH NAME
libmawk_set_cell \- set the value of a mawk cell.
.SH SYNOPSIS
.nf
.B #include <libmawk.h>
.sp
.BI "CELL *libmawk_set_cell(mawk_state_t *" m ", CELL *" cell ", const char" argtype "," ... );
.fi
.BI "CELL *libmawk_set_cellp(mawk_state_t *" m ", CELL *" cell ", const char" argtype ", void *" argp );
.sp
.SH DESCRIPTION
The
.BR libmawk_set_cell ()
function modifies the value of a mawk cell (variable). Argumetn argtype is a
format character that describes the type of the payload (accessed trough vararg).
.sp
The
.BR libmawk_set_cellp ()
function performs the same action but accepts a generic pointer to the payload.
.sp
.B "Format character"
is one of the followings:
.in +4n
.TP
 'd' for int payload
.TP
 'f' for double payload
.TP
 's' for (zero terminated) char * payload.
.in
.sp
Argument m is a libmawk context previously returned by libmawk_initialize()
or libmawk_initialize_stage3().

.SH "RETURN VALUE"
A pointer to the cell modified.
.SH "SEE ALSO"
.BR libmawk_initialize_stage (3libmawk),
.BR libmawk_initialize (3libmawk),
.BR libmawk_get_var (3libmawk).
