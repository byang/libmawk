#!/bin/sh
docdir=../../../doc
PAGES="example.7libmawk
libmawk_append_input.3libmawk
libmawk_call_function.3libmawk
libmawk_cell_destroy.3libmawk
libmawk_get_var.3libmawk
libmawk_initialize.3libmawk
libmawk_initialize_stage.3libmawk
libmawk_register_function.3libmawk
libmawk_run_main.3libmawk
libmawk_set_cell.3libmawk
libmawk_uninitialize.3libmawk
lmawk.1"

(
	cat input/example1
	sed "s/\\\\/\\\\\\\\/g;s/\t/  /g" < ../../example_apps/10_run/app.c
	cat input/example2
) > example.7libmawk

for n in $PAGES
do
	groff -c -Tlatin1 -mandoc $n > $docdir/$n.txt
	groff -c -Thtml -mandoc $n > $docdir/$n.html
done

