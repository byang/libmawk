#ifndef MAWK_EXECUTE_H
#define MAWK_EXECUTE_H

typedef enum mawk_exec_result_e {
	MAWK_EXER_FUNCRET,       /* function return, return value is on top of the stack */
	MAWK_EXER_DONE,          /* done with BEGIN or END */
	MAWK_EXER_EXIT,          /* script ran exit */
	MAWK_EXER_INT_READ,      /* was running BEGIN, END, main or function, got blocked in a read */
	MAWK_EXER_INT_RUNLIMIT,  /* was running BEGIN, END, main or function, reached run limit */
	MAWK_EXER_INT_SEPMAIN,   /* about to jump on main but separate exetuion of main is configured */
	MAWK_EXER_ERR_NOSTACK    /* can't resume: nothing's on the stack */
} mawk_exec_result_t;


/* resume execution of an interrupted script */
mawk_exec_result_t mawk_resume(mawk_state_t *MAWK);

mawk_exec_result_t mawk_call(mawk_state_t * MAWK, FBLOCK * fbp, int a_args, mawk_cell_t *res);
int mawk_test(mawk_state_t *MAWK, register mawk_cell_t *cp);

#endif
