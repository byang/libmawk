#ifndef DA_BIN_H
#define DA_BIN_H
#include  "mawk.h"

/* Load precompiled binary script from a file */
int mawk_load_code_bin(mawk_state_t *MAWK, const char *fn);

/* Print precompiled binary from da to stdout */
int mawk_print_code_bin(mawk_state_t *MAWK, const char *fn);

#endif
