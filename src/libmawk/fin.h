
/********************************************
fin.h

libmawk changes (C) 2009-2012, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

/* buffered, splitting input (FIN, aka mawk_input_t) */

#ifndef  FIN_H
#define  FIN_H
/* structure to control input files */

enum {
	MAWK_INPF_MAIN               = 1,   /* part of main input stream if on */
	MAWK_INPF_EOF                = 2,   /* reached EOF */
	MAWK_INPF_START              = 4,   /* used when RS == "" */
	MAWK_INPF_NO_MORE            = 8,   /* there's no more input */
	MAWK_INPF_DEAD_NO_FREE       = 16,  /* static dead buffer - do not free */
	MAWK_INPF_NO_MORE_INPUTS     = 32   /* we are at the end of the input file list, impossible to have any more main input */
};

struct mawk_fin_s {
	FILE_NODE *fn;
	char *buf;
	char *next;            /* start of the next record, within buf; anything before this is already returned in a previous call */
	int used;              /* how much bytes are in use in buf - this includes records already used up in the beginning of the buffer */
	int alloced;           /* total allocated size of the buf */
	int flags;
};


#define mawk_FIN_nomore (-2)

mawk_input_t *mawk_fin_alloc(mawk_state_t *MAWK, FILE_NODE *parent);
void mawk_fin_free(mawk_state_t *MAWK, mawk_input_t *fin);

long mawk_fillbuff(mawk_state_t *, mawk_input_t *, char *, unsigned, int interactive);

/* execution: */
void mawk_FINopen_main(mawk_state_t * MAWK);
char *mawk_FINgets(mawk_state_t *MAWK, FILE_NODE *fn, unsigned *len_p);

#endif /* FIN_H */
