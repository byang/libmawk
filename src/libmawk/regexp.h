
/********************************************
regexp.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/


PTR mawk_REcompile(mawk_state_t *MAWK, char *);
int mawk_REtest(mawk_state_t *MAWK, char *, PTR);
char *mawk_REmatch(mawk_state_t *MAWK, char *, PTR, unsigned *, int);
#ifdef DEBUG
#include <stdio.h>
void mawk_REmprint(PTR m, FILE *f);
#endif

extern const char *mawk_REerrlist[];
