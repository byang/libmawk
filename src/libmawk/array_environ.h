/* create a blank array reserved for ENVIRON[] */
void mawk_environ_init(mawk_state_t *MAWK);

#ifdef MAWK_MEM_PEDANTIC
void mawk_environ_uninit(mawk_state_t *MAWK);
#endif

/* allocate memory and return an array suitable for env arg of execle();
   allocation is done using mawk_malloc() to ensure a large ENVIRON[] doesn't
   cause excess memory allocation (ram limit!). However, this allocation
   is not to be free'd ever, since an exec() or exit() follows the call.
   Thus if there's no change to the ENVIRON[] array, a pointer to
   environ is returned and no allocation is done.

   Returns NULL on error
*/
char **mawk_environ_extract(mawk_state_t *MAWK);
