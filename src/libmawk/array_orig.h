/*
libmawk (C) 2014, Tibor 'Igor2' Palinkas;

libMawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
*/

#ifndef ARRAY_ORIG_H
#define ARRAY_ORIG_H 1

/* the original mawk array implementation, large, cryptic and well optimized */
extern array_imp_t mawk_array_orig_imp;


/* iterator - used by custom array implementations built on top of array_orig */
void *mawk_array_it_start_orig(mawk_state_t *MAWK, mawk_array_t A);
const mawk_cell_t *mawk_array_it_next_orig(mawk_state_t *MAWK, mawk_array_t A, void *iterator);
void mawk_array_it_stop_orig(mawk_state_t *MAWK, mawk_array_t A, void *iterator);

#endif /* ARRAY_ORIG_H */
