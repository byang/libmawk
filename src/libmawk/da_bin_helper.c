/********************************************
libmawk (C) 2009-2014, Tibor 'Igor2' Palinkas;

This is a source file for libmawk, an implementation of
the AWK programming language, fork of mawk.

Libmawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "da_bin.h"

static int bin_read(void *fd, void *buff, size_t size)
{
	return read(*(int *)fd, buff, size);
}

int mawk_load_code_bin(mawk_state_t *MAWK, const char *fn)
{
	int ret, fd;

	fd = open(fn, O_RDONLY);
	if (fd < 0)
		return MAWK_ECANTOPEN;

	ret = mawk_load_code_bin_(MAWK, &fd, bin_read);

	close(fd);
	return ret;
}

static int bin_write(void *fd, const void *buff, size_t size)
{
	return write(*(int *)fd, buff, size);
}

int mawk_print_code_bin(mawk_state_t *MAWK, const char *fn)
{
	int ret, fd;

	fd = 1;
	ret = mawk_save_code_bin_(MAWK, &fd, bin_write);
	fsync(1);

	return ret;
}

