
/********************************************
jmp.h

libmawk changes (C) 2009-2010, Tibor 'Igor2' Palinkas;
based on mawk code coming with the below copyright:

copyright 1991, Michael D. Brennan

This is a source file for mawk, an implementation of
the AWK programming language.

Mawk is distributed without warranty under the terms of
the GNU General Public License, version 2, 1991.
********************************************/

#ifndef   JMP_H
#define   JMP_H

void mawk_BC_new(mawk_state_t *);
void mawk_BC_insert(mawk_state_t *, int, INST *);
void mawk_BC_clear(mawk_state_t *, INST *, INST *);
void mawk_code_push(mawk_state_t *, INST *, unsigned, int, FBLOCK *);
unsigned mawk_code_pop(mawk_state_t *, INST *);
void mawk_code_jmp(mawk_state_t *, int, INST *);
void mawk_patch_jmp(mawk_state_t *, INST *);

#endif /* JMP_H  */
