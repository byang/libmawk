#include <stdio.h>
#include <libmawk.h>

/*
	Purpose: register "/dev/foo" to a custion vio that does calculates some
	         sort of hash from all data written to it.
	         The hash files are set up in a static manner: once the script
	         closes them or the files get eof from the app, they can not
	         be reopened to be the same virtual files (instead a reopen
	         would call the normal file open procedure)
	Run: ./app -f test.awk
*/

int main(int argc, char **argv)
{
	mawk_state_t *m;
	mawk_vio_t *vf;

	/* init a context in stages */
	m = libmawk_initialize_stage1();              /* alloc context */

	/* set up all pipes */
	libmawk_initialize_stdio(m, 0, 1, 1);

	vf = mawk_vio_hash_open(m, NULL, MAWK_VIO_O_APPEND); /* create a pipe for stdout */
	mawk_file_register(m, "/dev/hash", F_APPEND, vf); /* register for write */
	mawk_file_register(m, "/dev/hash", F_IN, vf);     /* register for read */

	printf("app: hash before begin: %d\n", mawk_vio_hash_val(m, vf));

	m = libmawk_initialize_stage2(m, argc, argv); /* set up with CLI arguments */
	m = libmawk_initialize_stage3(m);             /* execute BEGIN */

	if (m == NULL) {
		fprintf(stderr, "libmawk_initialize failed, exiting\n");
		return 1;
	}

	printf("app: hash after begin: %d\n", mawk_vio_hash_val(m, vf));

	/* run END */
	libmawk_uninitialize_stage1(m);

	printf("app: hash after end: %d\n", mawk_vio_hash_val(m, vf));

	/* need to release the app end of the deal to get everything free'd */
	mawk_vio_hash_eof_from_app(m, vf);

	/* free things */
	libmawk_uninitialize_stage2(m);

	return 0;
}

