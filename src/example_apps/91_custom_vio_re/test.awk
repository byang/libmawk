BEGIN {
	print "script: BEGIN test.awk"
	print "Hello world!" > "/dev/hash/4"
	print "Hello universe!" > "/dev/hash/6"
}

END {
	print "end." > "/dev/hash/4"
	print "END!" > "/dev/hash/6"
	getline val4 < "/dev/hash/4"
	getline val6 < "/dev/hash/6"
	print "script: END: " val4, val6

#	works even after reopeninig:
	close("/dev/hash/6")
	getline val6 < "/dev/hash/6"
	print "script: END: second time 6:"  val6

# reopen
	close("/dev/hash/6")
	print "new thing" > "/dev/hash/6"
	getline val6 < "/dev/hash/6"
	print "script: END: third time 6:"  val6
}
