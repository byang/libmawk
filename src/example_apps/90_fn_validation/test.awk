BEGIN {
	print "script: BEGIN test.awk"

# this will work with as expected
	getline line < "test.awk"
	print "script: read=" line

# this will be redirected to out.txt instead of "some_file"
	print "hello" > "some_file"

# this won't read the date because it's wrapper in an echo
	"date +%s" | getline line
	print "script: date=" line
}

