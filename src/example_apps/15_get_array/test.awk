BEGIN {
	print "script: BEGIN"
	bar["wow"] = 42
}

{
	print "script: input: \"" $0 "\""
	bar["wow"]++
}

END {
	print "script: END"
}
