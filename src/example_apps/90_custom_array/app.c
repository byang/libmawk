#include <stdio.h>
#include <libmawk.h>
#include "carr.h"

/*
	Purpose: create a virtual array that directly manipulates a C char[] without
	         any backing awk array. The actual array implementation is in carr.c
	Run: ./app -f test.awk
*/

int main(int argc, char **argv)
{
	mawk_state_t *m;
	int n;

	m = libmawk_initialize_stage1();               /* set up m */

	/* set up all pipes */
	libmawk_initialize_stdio(m, 0, 1, 1);

	/* set up a new builtin array with side effects, before parsing scripts
	   so that it works from BEGIN */
	custom_array_init(m);

	m = libmawk_initialize_stage2(m, argc, argv);  /* parse args loads the script(s) */
	m = libmawk_initialize_stage3(m);              /* execute BEGIN {} */

	/* Print the current state of the array */
	printf("app: CARR[]=\n");
	for(n = 0; n < CARR_SIZE; n++)
		printf("app:  [%d]=%d '%c'\n", n, carr[n], carr[n]);


	/* run END and free the context */
	libmawk_uninitialize(m);

	return 0;
}

