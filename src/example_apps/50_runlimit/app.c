#include <stdio.h>
#include <libmawk.h>

/*
	Purpose: run the script in small portions, only a few instruction at a time
	         using runlimit
	Run: ./app
*/

int main(int argc, char **argv)
{
	mawk_state_t *m;
	int n;

	/* init a context in stages */
	m = libmawk_initialize_stage1();             /* alloc context */
	libmawk_initialize_stdio(m, 0, 1, 1);        /* set up default stdio: stdin is a pipe, stdout and stderr are bound to the app's stdout and stderr with no-close-on-exit */
	mawk_append_input_file(m, "test.awk", 0);    /* force load test.awk */
	m = libmawk_initialize_stage2(m, 0, NULL);   /* set up with no arguments */
	m->runlimit = 32;                            /* run 32 instructions at a time */
	m = libmawk_initialize_stage3(m);            /* start executing BEGIN */


	if (m == NULL) {
		fprintf(stderr, "libmawk_initialize failed, exiting\n");
		return 1;
	}

	/* resume a few times */
	for(n = 0; n < 8; n++) {
		printf("app: interrupt\n");
		mawk_resume(m);
	}

	/* run END and free the context */
	libmawk_uninitialize(m);

	return 0;
}

