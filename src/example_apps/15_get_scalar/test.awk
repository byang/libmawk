BEGIN {
	print "script: BEGIN"
	bar = 42
}

{
	print "script: input: \"" $0 "\""
	bar++
}

END {
	print "script: END"
}
