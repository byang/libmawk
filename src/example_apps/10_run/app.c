#include <stdio.h>
#include <libmawk.h>

/*
	Purpose: load and run a script using the command
	         line syntax of mawk but using a virtual
	         stdin buffer instead of the real stdin.
	Run: ./app -f test.awk
*/

int main(int argc, char **argv)
{
	mawk_state_t *m;

	/* init a context, execute BEGIN */
	m = libmawk_initialize(argc, argv);
	if (m == NULL) {
		fprintf(stderr, "libmawk_initialize failed, exiting\n");
		return 1;
	}

	/* feed in some data on the virtual stdin */
	libmawk_append_input(m, "This is a\nmultiline test input\nfor the artificial input buffer.\n");

	/* run the MAIN part of the script as long as
	   there's data in the buffer of the virtual stdin */
	libmawk_run_main(m);

	/* run END and free the context */
	libmawk_uninitialize(m);

	return 0;
}

