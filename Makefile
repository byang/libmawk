all: src/libmawk/Makefile.conf
	cd src && $(MAKE)

install: src/libmawk/Makefile.conf
	cd src && $(MAKE) install
	cd doc && $(MAKE) install

uninstall:
	cd src && $(MAKE) uninstall

linstall: src/libmawk/Makefile.conf
	cd src && $(MAKE) linstall

clean:
	cd src && $(MAKE) clean

distclean:
	cd src && $(MAKE) distclean
	cd scconfig && $(MAKE) clean

test:
	cd src && $(MAKE) test

src/libmawk/Makefile.conf:
	@echo "Please run ./configure first."; false
