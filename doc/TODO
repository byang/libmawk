packging:
	- manual page headers use the wrong section

000. full doc rewrite

Features:
	- c->awk func call should be able to pass arrays

00. vio rewrite
	- add all vio in a linked list in MAWK for garbage collection at mawk_uninit
		- add a force-clenup hook for this; but also replace mawk_close_out_pipes()
	- hooks? wrapper?
	- document file_name_rewrite
	- document vio

0. bugs
	- string ref crash:
	    for(n = 0; n < 1000000; n++) A[n] = "foo"
	    if refcount of "foo" reaches 65535, it crashes
	- mpow2 should be static

1. restrictions
	- detect and use LDFLAGS -dynamic
	- split compile and run into separate libs:
		- implement a Single Safe number->string converter;
		  grep for OFMT and CONVFMT and INT_FMT to find all the buggy sprintf based
		  implementations
		- split da_bin to exec and comp
		- fix error.c: it shouldn't depend on stdio.h and it shouldn't print
		  to stderr anyway (incompatible with the lib idea)
	- check whether int-compiled lmawk handles OFMT/CONVFMT properly
	- floating point:
		- try to find a platform with FPE for overflow and test
		- consider a -W nandebug option so that the user knows where the script
		  went wrong; but he could also just check from the script
		- if isinf() is avaialable, check for inf() result and convert them to nan


1.5.
	- check all zmalloc() and zrealloc() calls - they may return NULL and callers
	  should return in that case so that runtime error takes over

2. porting
	- test on UNIX
	- provide a real alternative to realpath()

3.0 extend arrays
	- array copy
	- array in array (for orig implementation only?)
	- length(array) as in gawk? POSIX: length() works on strings only
	  update test_3rd funlen accordingly!

3.1 features, minor bugs
	- debugging (gdb scripts); location-file instruction to track src file changes
	- consider printf with no arg to work like in gawk; also check posix about this
	  test_3rd: printf0
	- decide whether regex "^+" (and "^*"?) should be accepted and treated as
	  plain + and * at the beginning of the string; update test_3rd reindops
	  (check posix regex)
	- decide whether regex should support binary; related tests to update:
	  test_3rd jared, regx8bit
	- posix FS point 3. requires that ^ work in FS; check test_3rd uparrfs
	- introduce a new symtab flag for remembering builtin vars and arrays;
	  when -W dumpsym, do not dump these unless verbose
	- introduce a -W dumpallsym (for verbose symdumps)
	- write regression test for flush() (it used to pass the wrong pointer)

4. lib fineprint
	- expose mawk_append_input_file

5. optimization
	- mawk_find_bi_ptr(): use perfect hash instead of linear search
	  does it really matter?
	- peephole:
		- 'print "a", a, "b", b, "c", c' results in push/cat pairs;
		  make the parser emit a lot of pushes and replace cat with catN
		- 'expr = 1' will add a pop; there should be another type of assignment
		  that doesn't push anything
		- replace tail recursion (call+ret)

